<?php
return [
    'Welcome to our application'       => 'Welcome to our application',
    /*Registration */
    'Register to'   =>  'Register to ',
    'Red Genie'     =>  'Red Genie',
    'Sales Genie'   =>  'Sales Genie',
    'Customer'      =>   'Customer',
    'Referral ID'   =>   'Referral ID',
    'Username'      =>   'Username',
    'Password'      =>   'Password',
    'Email Id'      =>   'Email Id',
    'Phone'         =>   'Phone',
    'REGISTER'      =>   'REGISTER',
    'You already have an account Please'=>'You already have an account Please',
    'LOGIN'         =>   'LOGIN',

    'Create Customer'  =>   'Create Customer',
    'Vendor'         =>   'Vendor',
    'Name'         =>   'Name',
    'Create Sales Genie'     =>   'Create Sales Genie',
    'Select Sales Genie'     =>   'Select Sales Genie',

    /*login */
    'LOGIN AS'      => 'LOGIN AS',
    'Remember me'   => 'Remember me',
    'Forget Password?'  => 'Forget Password?',
    'Dont have an account Please' => 'Dont have an account Please',
    'SIGN UP' => 'SIGN UP',

    /* Forgot Password */
    'Forgot Password' => 'Forgot Password',
    'SUBMIT' => 'SUBMIT',

    /* profile */
    'Experience' => 'Experience',
    'State of Qatar' => 'State of Qatar',
    'Upload Image'  => 'Upload Image',
    'Select image to crop' => 'Select image to crop',
    'save' => 'save',

    'Add'  => 'Add',
    'Cancel' => 'Cancel',
    'Remove' => 'Remove',
    'Designation' => 'Designation',
    'Description' => 'Description',

    /* menus */
    'Home'  => 'Home',
    'Resource Centre' => 'Resource Centre',
    'Statements' => 'Statements',
    'Invite Customers' => 'Invite Customers',
    'Create Sales Genies' => 'Create Sales Genies',
    'Logout' => 'Logout',


     /* statements */
     'Point'  => 'Point',
     'Credit' => 'Credit',
     'Red Credits' => 'Red Credits',
     'Red Points' => 'Red Points',
     'Amount'     => 'Amount',
     'Total'      => 'Total',
     'Date'       => 'Date',
     'Customer code' => 'Customer code',
     'From' => 'From',
     'To' => 'To',

     /* Resource center */
     'Featured'  => 'Featured',
     'Training Manual' => 'Training Manual',
     'Articles' => 'Articles',
     'Case Studies' => 'Case Studies',
     'Videos'     => 'Videos',
     'Read Training Manual'      => 'Read Training Manual',
     'Watch the video'       => 'Watch the video',
     'SEE THE CASE STUDY' => 'SEE THE CASE STUDY',
     'Read the article' => 'Read the article',

     'Training manual' => 'Training manual',
     'ARTICLE' => 'ARTICLE',
     'CASE STUDIES' => 'CASE STUDIES',
     'VIDEO'     => 'VIDEO',

     /* network page */
     'my network'  => 'my network',
     'Sales Genies'  => 'Sales Genies',
     'customers'  => 'customers',
     'No sales genie'  => 'No sales genie were registered',

     'No customers'  => 'No customers were registered',
     
     'more'  => 'More',
     



    /* Footer */
    'Terms of Use' => 'Terms of Use',
    'Privacy Policy' => 'Privacy Policy',
];



?>
