<?php
return [
    'Welcome to our application'       => 'مرحبا بكم في تطبيقنا',
    /*Registration */
    'Register to'   =>  'سجل ل',
    'Red Genie'     =>  'الجني الأحمر',
    'Sales Genie'   =>  'جني المبيعات',
    'Customer'      =>   'العملاء',
    'Referral ID'   =>   'معرف الإحالة',
    'Username'      =>   'اسم المستخدم',
    'Password'      =>   'كلمه السر',
    'Email Id'      =>   'عنوان الايميل',
    'Phone'         =>   'رقم الهاتف',
    'REGISTER'      =>   'تسجيل',
    'You already have an account Please'=>'لديك بالفعل حساب من',
    'LOGIN'         =>   'الدخول تسجيل',

    'Create Customer'  =>   'إنشاء عميل',
    'Vendor'         =>   'بائع',
    'Name'         =>   'اسم',
    'Create Sales Genie'     =>   'إنشاء جني المبيعات',
    'Select Sales Genie'     =>   'اختر جني المبيعات',

    /*login */
    'LOGIN AS'      => 'سجل دخول',
    'Remember me'   => 'تذكرنى',
    'Forget Password?'  => 'نسيت كلمة المرور؟',
    'Dont have an account Please' => 'لا تملك حساب من فضلك',
    'SIGN UP' => 'سجل',

    /* Forgot Password */
    'Forgot Password' => 'هل نسيت كلمة المرور',
    'SUBMIT' => 'إرسال',

    /* profile */
    'Experience' => 'تجربة',
    'State of Qatar' => 'دولة قطر',
    'Upload Image'  => 'تحميل الصور',
    'Select image to crop' => 'حدد صورة لاقتصاصها',
    'save' => 'حفظ',

    'Add'  => 'أضف',
    'Cancel' => 'إلغاء',
    'Remove' => 'إزالة',
    'Designation' => 'تعيين',
    'Description' => 'وصف',

    /* menus */
    'Home'  => 'الصفحة الرئيسية',
    'Resource Centre' => 'مركز الموارد',
    'Statements' => 'صياغات',
    'Invite Customers' => 'قم بدعوة العملاء',
    'Create Sales Genies' => 'إنشاء جينات المبيعات',
    'Logout' => 'تسجيل خروج',


    /* statements */
    'Point'  => 'نقطة',
    'Credit' => 'ائتمان',
    'Red Credits' => 'الاعتمادات الحمراء',
    'Red Points' => 'النقاط الحمراء',
    'Amount'     => 'كمية',
    'Total'      => 'مجموع',
    'Date'       => 'تاريخ',
    'Customer code' => 'كود العميل',
    'From' => 'من عند',
     'To' => 'إلى',


      /* Resource center */
      'Featured'  => 'متميز',
      'Training Manual' => 'دليل التدريب',
      'Articles' => 'مقالات',
      'Case Studies' => 'دراسات الحالة',
      'Videos'     => 'أشرطة فيديو',
      'Read Training Manual'      => 'اقرأ دليل التدريب',
      'Watch the video'       => 'شاهد الفيديو',
      'SEE THE CASE STUDY' => 'انظر دراسة الحالة',
      'Read the article' => 'اقرأ المقال',

      'Training manual' => 'دليل التدريب',
     'ARTICLE' => 'مقالة - سلعة',
     'CASE STUDIES' => 'دراسات الحالة',
     'VIDEO'     => 'فيديو',

     /* network page */
     'my network'  => 'شبكتي',
     'Sales Genies'  => 'جينات المبيعات',
     'customers'  => 'الزبائن',
     'No sales genie'  => 'لم يتم تسجيل جني المبيعات',

     'No customers'  => 'لم يتم تسجيل عملاء',
     
     'more'  => 'أكثر',
     




    /* Footer */
    'Terms of Use' => 'تعليمات الاستخدام',
    'Privacy Policy' => 'سياسة الخصوصية',
];
?>
