<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table class="table table-bordered" style="width:100%;">
    <thead>
      <tr>
      <td><b>User Code</b></td>
        <td><b>User Name</b></td>
        <td><b>Refferal/Customer Code</b></td>
        <td><b>Type</b></td> 
        <td><b>Point</b></td>
        <td><b>Date</b></td>     
      </tr>
      </thead>
      <tbody>
      @foreach($points as $credit)
						   @if($credit->code != '')
      <tr>
      <td>
          {{$credit->code}}
        </td>
        <td>
        {{$credit->name}}
        </td>
        <td>
        {{$credit->refferal_code}}
        </td>
        <td>
        <strong>@if($credit->type == 1) Credit  @endif
                @if($credit->type == 2) Point  @endif
                @if($credit->type == 3) Redumption  @endif
                </strong>
        </td>
        <td>
        {{$credit->point}}
        </td>
        <td>
        {{$credit->created_at}}
        </td>
      </tr>
      @endif

      @endforeach
      </tbody>
    </table>
  </body>
</html>