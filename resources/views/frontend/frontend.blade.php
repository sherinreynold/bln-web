<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('frontend/css/bootstrap.css')}}">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{asset('frontend/css/semantic.min.css')}}">
<!--<link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">-->
@php
if (App::isLocale('en')) {
	@endphp
	<link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
	@php
}
@endphp

@php
if (App::isLocale('ar')) {
	@endphp
	<link rel="stylesheet" href="{{asset('frontend/css/style_rtl.css')}}">
	@php
}
@endphp

<link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">
  

<title>Refer to Gain</title>
</head>
<body>

@include('frontend.shared.header') 		
		
          @yield('content') 

        </main>
      </div>
      @include('frontend.shared.footer')
    </div>



   <!-- jQuery first, then Popper.js, then Bootstrap JS --> 
<script src="{{asset('frontend/js/jquery-3.2.1.min.js')}}"></script> 
<script src="{{asset('frontend/js/popper.js')}}"></script> 
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script> 

    @yield('javascript')




  </body>
</html>
