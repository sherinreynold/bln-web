
@extends('frontend.frontend')

@section('content')

<style>
.tab_box
{
width:258px;
height:256px ;
margin-top: 24px;
padding: 23px 18px;
border-radius: 17px;
float: left;
margin-right: 22px;
background-size: cover;
background-repeat: no-repeat;
background-position: bottom center;
}
#tab-button li {
 display: inline-block;
    padding-right: 50px;
}
</style>

<section class="resource_tab">
  
    <div class="container">
      <div class="row">
<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
<div class="tabs">
  <div class="tab-button-outer">
    <ul id="tab-button" class="rse_tab">
      <li><a href="#tab01">{{ __('messages.Featured') }}</a></li>
      <li><a href="#tab02">{{ __('messages.Training Manual') }}</a></li>
      <li><a href="#tab03">{{ __('messages.Articles') }}</a></li>
      <li><a href="#tab04">{{ __('messages.Case Studies') }}</a></li>
      <li><a href="#tab05">{{ __('messages.Videos') }}</a></li>
    </ul>
  </div>
  
  </div>
  
  




  <div id="tab01" class="tab-contents">
 <div id="style_tab" class="scrollbar">   

 @foreach ($rc_featured as $rct)

@if($rct->type=='1')
<div class="tab_box training_bg">
<p class="tab_title">{{ __('messages.Training manual') }}</p>
<p class="tab_note">{{$rct->title}} …</p>
<a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.Read Training Manual') }}</button></a>
</div>
@endif

@if($rct->type=='4')
<div class="tab_box vdo_bg">
<p class="tab_title">{{ __('messages.VIDEO') }}</p>
<p class="tab_note">{{$rct->title}} …</p>
<a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.Watch the video') }}</button></a>
</div>
@endif

@if($rct->type=='3')
<div class="tab_box case_bg">
<p class="tab_title">{{ __('messages.CASE STUDIES') }}</p>
<p class="tab_note">{{$rct->title}} …</p>
<a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.SEE THE CASE STUDY') }}</button></a>
</div>
@endif

@if($rct->type=='2')
<div class="tab_box article_bg">
<p class="tab_title">{{ __('messages.ARTICLE') }}</p>
<p class="tab_note">{{$rct->title}} …</p>
<a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.Read the article') }}</button></a>
</div>
@endif

@endforeach


  </div></div>






  <div id="tab02" class="tab-contents">
     <div id="style_tab" class="scrollbar">   

       @foreach ($rc_training_manual as $rct)
       <div class="tab_box training_bg">
       <p class="tab_title">{{ __('messages.Training manual') }}</p>
       <p class="tab_note">{{$rct->title}} …</p>
      <a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.Read Training Manual') }}</button></a>

       </div>
       @endforeach

      </div>
  </div>
  
  
  
  
  
  
  <div id="tab03" class="tab-contents">
    <div id="style_tab" class="scrollbar">   
     
    @foreach ($rc_article as $rct)
     <div class="tab_box article_bg">
     <p class="tab_title">{{ __('messages.ARTICLE') }}</p>
     <p class="tab_note">{{$rct->title}} …</p>
     <a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.Read the article') }}</button></a>
     </div>
    @endforeach 

    </div>
  </div>








  <div id="tab04" class="tab-contents">
    <div id="style_tab" class="scrollbar">

    @foreach ($rc_case_studies as $rct)
      <div class="tab_box case_bg">
      <p class="tab_title">{{ __('messages.CASE STUDIES') }}</p>
      <p class="tab_note">{{$rct->title}} …</p>
      <a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.SEE THE CASE STUDY') }}</button></a>

      </div>
      @endforeach


    </div>
  </div>






  <div id="tab05" class="tab-contents">
    <div id="style_tab" class="scrollbar">   

    @foreach ($rc_videos as $rct)
    <div class="tab_box vdo_bg">
    <p class="tab_title">{{ __('messages.VIDEO') }}</p>
    <p class="tab_note">{{$rct->title}} …</p>
    <a href="{{$rct->link}}" class="view_resource"><button class="tab_btn">{{ __('messages.Watch the video') }}</button></a>

    </div>
    @endforeach

   </div>
  </div>




</div>

<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3">

<p class="rse_title"><!--View--></p>
<embed id="embed" src="" width="450" height="600" alt="pdf" type="video/webm">
<!--<p class="rse_note"> </p>  {{asset('upload/resources/test.pdf')}}-->


</div>



</div>
</div>
  </div>

</section>

@endsection


@section('javascript')
 
<script>
$(function() {
  var $tabButtonItem = $('#tab-button li'),
      $tabSelect = $('#tab-select'),
      $tabContents = $('.tab-contents'),
      activeClass = 'is-active';

  $tabButtonItem.first().addClass(activeClass);
  $tabContents.not(':first').hide();

  $tabButtonItem.find('a').on('click', function(e) {
    var target = $(this).attr('href');

    $tabButtonItem.removeClass(activeClass);
    $(this).parent().addClass(activeClass);
    $tabSelect.val(target);
    $tabContents.hide();
    $(target).show();
    e.preventDefault();
  });

  $tabSelect.on('change', function() {
    var target = $(this).val(),
        targetSelectNum = $(this).prop('selectedIndex');

    $tabButtonItem.removeClass(activeClass);
    $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
    $tabContents.hide();
    $(target).show();
  });
});

$(document).ready(function(){
	
	$(".view_resource").on("click",function(){
		var atr=$(this).attr('href');
		$("#embed").attr('src', atr);
		return false;
	});
});
</script>

@endsection