
@extends('frontend.frontend')

@section('content')
<style>
.hidden
{
	display:none;
}
</style>

<section class="profile_sectn">
  
    <div class="container">
	<form id="login-form" class="form" action="{{url('/profile_update')}}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"> <img src="{{asset('frontend/img/profile_pic.jpg')}}" class="pro_pic img-fluid">
        <p class="name">Red Genie</p> 
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="row">
<div class="pro_detai1">
<p class="pro_name"><span class="show">{{$user['first_name']}}</span >
<input type="text" name="name" id="name" class="form-control login_main_input hidden" placeholder="Name" value="{{$user['first_name']}}" required>
</p>
<p class="pro_mail"><span class="show">{{$login_info['email']}}</span>
<input type="text" name="email" id="email" class="form-control login_main_input hidden" placeholder="Email" value="{{$login_info['email']}}" required>

</p>
<p class="pro_detail"><span class="show">{{$user['address']}}
</span>
<textarea name="address" id="address" class="form-control hidden" placeholder="Address" required style="width: 82% !important;">{{$user['address']}} </textarea>

</p>     
</div>
</div>

 </div>
 
<div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
 <div class="pro_cnt_detai1">
<img onclick="edit();"src="{{asset('frontend/img/edit.jpg')}}" class="edit_icon show">
<input type="submit" value="save" class="login_btn hidden" style="float:right;width: 100% !important;height: 37px !important;">
<p class="mob_no"><span class="mob_icon"><img src="{{asset('frontend/img/mob_icon.png')}}" class="img-fluid" ></span> <span class="no"><span class="show">+{{$user['mobile']}}</span>
<input required type="text" name="phone" id="phone" class="form-control login_main_input hidden" placeholder="Mobile" value="{{$user['mobile']}}" style="margin-top: -7px !important;
width: 100% !important;">
 </span></p>
 <p class="country"></p>
 <p class="cntry_no"><span class="cntry_icon"><img src="{{asset('frontend/img/flag.png')}}" class="img-fluid"  ></span> <span class="cntry">State of Qatar</span> </p>
 <p class="country"></p>      
      </div>
</div>

</div>
</form>



  </div>

</section>
<!--============profile_sectn============-->
<!--============experience_sectn============-->
<section class="experience">
  
    <div class="container">
      <div class="row">
<div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2"></div>
<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
<div class="experience_sectn">
<p class="exp_title">Experience<span class="exp_icon"><img src="{{asset('frontend/img/edit.jpg')}}" class="img-fluid"  ></span></p>
<div class="exp_btm_sectn">
<p class="exp_btm_title">Founder & Managing Director</p>
<p class="exp_btm_dscptn founder">Entrepreneur successfully set up a GCC wide logistics business in 10 years</p>
<span><img src="{{asset('frontend/img/red_dpdwn.png')}}" class="acrdn"  ></span>
</div>

<div class="exp_btm_sectn">
<p class="exp_btm_title">Mentor & Director</p>
<p class="exp_btm_dscptn founder">Entrepreneur successfully set up a GCC  wide logistics business in 10   </p> <div id="matrix_content">
  <p class="exp_btm_dscptn_1 founder">0 years &  involved in the…  </p>
</div>
<span><img src="{{asset('frontend/img/red_dpdwn.png')}}" class="acrdn" id="max_min"   ></span>
</div>

<div class="exp_btm_sectn brdr_none">
<p class="exp_btm_title">Mentor & Director</p>
<p class="exp_btm_dscptn founder">Entrepreneur successfully set up a GCC  wide logistics business in 10 years &  involved in the…  </p>
<span><img src="{{asset('frontend/img/red_dpdwn.png')}}" class="acrdn"  ></span>
</div>

</div>
</div>

</div>
  </div>

</section>

<script>
function edit()
{
	$('.hidden').show();
	$('.show').hide();
}
</script>

@endsection


@section('javascript')

@endsection