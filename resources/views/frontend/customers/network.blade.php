<style type="text/css">
  @media only screen and (max-width: 600px) {
    .middile {
      border-left: none!important; 
      border-right: none!important;
    }
    .network_third_main_subcst{
      padding-left: 0px !important;
    }
  }
  .first_lvl{
    width: 71px !important;
    height: 71px !important;
    margin-left: auto !important;
    margin-right: auto !important;
    display: block !important;
    margin-top: 0px !important;
  }
  .second_lvl{
    width: 56px !important;
    height: 56px !important;
    margin-left: auto !important;
    margin-right: auto !important;
    display: block !important;
    margin-top: 0px !important;
  }
  .third_lvl{
    width: 40px !important;
    height: 40px !important;
    margin-left: auto !important;
    margin-right: auto !important;
    display: block !important;
    margin-top: 0px !important;
  }
  .customer_lvl{
    width: 32px !important;
    height: 32px !important;
    margin-left: auto !important;
    margin-right: auto !important;
    display: block !important;
    margin-top: 0px !important;
  }
  .nt_sub_name{
    height: 73px;
  }
  .no-border{
    border:none!important;
  }
  .line_btm_fst, .line_btm_scnd, .line_btm, .line_btm_frth, .line_btm_thrd, .line_btm_ffth{
    height: 76px !important;
  }
  /*romove*/
  .network_third_main_cusonly {
    max-width: 50% !important;
    float: right;
    height: auto;
  }
  /*end remove*/
  .nav-link-custom{
    pointer-events: none!important;
    background:none !important;
    border:none!important;
  }
  .main_div {
    display: block !important;
    ;
    min-width: none !important;
    float: none !important;
  }
  @media only screen and (min-width: 993px) {
    .third_sub {
      width: 109px !important;
    }
    .network_third_main_sub_cusonly{
      width: 600px !important;
    }
  }
  .third_margin{
    margin-left: 281px !important;
  }
  .line_down_right_third{
    border-bottom: none !important;
  }
  .linemain_1_third{
    padding-left: 48% !important;
  }
  .third_div{
    float: right !important;
  }
  .line_down_right_1_ffth_custom{
    float: right !important;
    border-bottom: none !important;
    width: 10.4% !important;
  }
  .line_down_right_1_fst_cstm{
    border-bottom:none!important;
  }
  .linemain_1_fst_cstm{
    padding-left: 9% !important;
  }
  .linemain_1_scnd_cstm{
    padding-left: 29% !important;
  }
  .network_third_main_sub_cstm{
    /*margin-left: 71px;*/
  }
  .line_down_right_1_scnd_cstm{
    border-bottom: none !important;
  }
  .line_down_right_1_frth_cstm{
    border-bottom: none !important;
  }
  .network_third_main_sub_cst{
    float: right !important;
  }
  .linemain_1_frth_cstm{
    padding-left: 69% !important;
  }
</style>
@extends('frontend.frontend')
@section('content')
<section class="network">
  <div class="container">
    <div class="row"
         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <p class="ntwk_title">{{ __('messages.my network') }}
    </p>
    <div class="btm_box_main">
      <div class="btm_box">
        <p class="btm_box_title">{{ __('messages.Sales Genies') }} + 
        </p>
        <p class="btm_box_number">{{ $res['sales_gen_p_tot'] }}
        </p>
      </div>
      <div class="btm_box middile">
        <p class="btm_box_title">{{ __('messages.Sales Genies') }}
        </p>
        <p class="btm_box_number">{{ $res['sales_gen_tot'] }}
        </p>
      </div>
      <div class="btm_box">
        <p class="btm_box_title">{{ __('messages.customers') }} 
        </p>
        <p class="btm_box_number">{{ $res['customers_tot'] }}
        </p>
      </div>
    </div>
  </div>
  </div>
</div>
</section>
<div class="table-responsive">
  <div class="main_div">
    <section class="network_digm">
      <div class="container">
        <div class="row">
          <div class="network_top"> 
            <!-- <img src="{{asset('frontend/img/1.png')}}" class="mx-auto d-block"> -->
                    @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['red_gn']['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['red_gn']['fk_frontend_user_id'].'.png')}}" class="pro_pic first_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avatar1.png')}}" class="pro_pic first_lvl img-fluid">
                    @endif
            <p class="nt_name">{{$res['red_gn']['first_name'].' '.$res['red_gn']['last_name']}}
            </p>
          </div>
        </div>
      </div>
    </section>
    <section class="linemain">
      <div class="container">
        <div class="line_down_left ">
        </div>
        <div class="line_down_right">
          <!-- Sales Genie+ -->&nbsp;
        </div>
      </div>
    </section>
    <section class="network_tab_main">
      <div class="container">
        <div class="row">
          <div class="col-xl-12 ">
            <nav>
              <div class="nav nav-tabs nav-fill cus-link" id="nav-tab" role="tablist">
                @if(count($res['sales_gn'])==0)
                <div style="border:none !important" class="network_second_sub_1">
                  <div class="second_sub" style="width: 100% !important">
                    <p>
                      <span class="small_text">{{ __('messages.No sales genie') }} 
                      </span>
                      <br>
                    </p>
                  </div>
                </div>
                @endif
                <!-- first tab -->
                @if($res['sales_gn'][3])
                <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                  <div class="network_second_sub">
                   <!--  <img src="{{asset('frontend/img/3.png')}}" class="mx-auto d-block"> -->

                    @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['fk_frontend_user_id'].'.png')}}" class="pro_pic second_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar3.png')}}" class="pro_pic second_lvl img-fluid">
                    @endif

                    <p class="nt_sub_name">{{ $res['sales_gn'][3]["first_name"]." ".$res['sales_gn'][3]["last_name"]  }}
                    </p>
                    @if($res['sales_gn'][3]["type"]=='2')
                    <div class="network_second_sub_1">
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.Sales Genies') }}
                          </span>
                          <br>
                          {{ $res['sales_gn'][3]['sales_gn_sub']['total_salesgen'] }} 
                        </p>
                      </div>
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][3]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @else
                    <div class="network_second_sub_1">
                      <div class="second_sub" style="width: 100% !important">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][3]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @endif
                  </div>
                </a> 
                @endif
                <!-- second tab -->
                @if($res['sales_gn'][1]) 
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">
                  <div class="network_second_sub"> 
                    <!-- <img src="{{asset('frontend/img/6.png')}}" class="mx-auto d-block"> -->

                      @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['fk_frontend_user_id'].'.png')}}" class="pro_pic second_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar3.png')}}" class="pro_pic second_lvl img-fluid">
                    @endif



                    <p class="nt_sub_name">{{ $res['sales_gn'][1]["first_name"]." ".$res['sales_gn'][1]["last_name"]  }}
                    </p>
                    @if($res['sales_gn'][1]["type"]=='2')
                    <div class="network_second_sub_1">
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.Sales Genies') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][1]['sales_gn_sub']['total_salesgen'] }} 
                        </p>
                      </div>
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][1]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @else
                    <div class="network_second_sub_1">
                      <div class="second_sub" style="width: 100% !important">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][1]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @endif
                  </div>
                </a> 
                @endif
                <!-- third tab -->
                @if($res['sales_gn'][0])
                <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                  <div class="network_second_sub">
                   <!--  <img src="{{asset('frontend/img/2.png')}}" class="mx-auto d-block"> -->

                     @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['fk_frontend_user_id'].'.png')}}" class="pro_pic second_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar3.png')}}" class="pro_pic second_lvl img-fluid">
                    @endif



                    <p class="nt_sub_name"> {{ $res['sales_gn'][0]["first_name"]." ".$res['sales_gn'][0]["last_name"]  }}
                    </p>
                    @if($res['sales_gn'][0]["type"]=='2')
                    <div class="network_second_sub_1">
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.Sales Genies') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][0]['sales_gn_sub']['total_salesgen'] }} 
                        </p>
                      </div>
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][0]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @else
                    <div class="network_second_sub_1">
                      <div class="second_sub" style="width: 100% !important">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][0]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @endif
                  </div>
                </a>
                @endif
                <!-- fourth tab -->
                @if($res['sales_gn'][2])
                <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">
                  <div class="network_second_sub"> 
                    <!-- <img src="{{asset('frontend/img/5.png')}}" class="mx-auto d-block"> -->

                      @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['fk_frontend_user_id'].'.png')}}" class="pro_pic second_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar3.png')}}" class="pro_pic second_lvl img-fluid">
                    @endif


                    <p class="nt_sub_name">{{ $res['sales_gn'][2]["first_name"]." ".$res['sales_gn'][2]["last_name"]  }}
                    </p>
                    @if($res['sales_gn'][2]["type"]=='2')
                    <div class="network_second_sub_1">
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.Sales Genies') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][2]['sales_gn_sub']['total_salesgen'] }} 
                        </p>
                      </div>
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][2]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @else
                    <div class="network_second_sub_1">
                      <div class="second_sub" style="width: 100% !important">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][2]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @endif
                  </div>
                </a>
                @endif
                <!-- fifth tab -->
                @if($res['sales_gn'][4])
                <a class="nav-item nav-link" id="nav-aboutnew-tab" data-toggle="tab" href="#nav-aboutnew" role="tab" aria-controls="nav-aboutnew" aria-selected="false">
                  <div class="network_second_sub"> 
                    <!-- <img src="{{asset('frontend/img/4.png')}}" class="mx-auto d-block"> -->
                    @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['fk_frontend_user_id'].'.png')}}" class="pro_pic second_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar3.png')}}" class="pro_pic second_lvl img-fluid">
                    @endif


                    <p class="nt_sub_name">{{ $res['sales_gn'][4]["first_name"]." ".$res['sales_gn'][4]["last_name"]  }}
                    </p>
                    @if($res['sales_gn'][4]["type"]=='2')
                    <div class="network_second_sub_1">
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.Sales Genies') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][4]['sales_gn_sub']['total_salesgen'] }} 
                        </p>
                      </div>
                      <div class="second_sub">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][4]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @else
                    <div class="network_second_sub_1">
                      <div class="second_sub" style="width: 100% !important">
                        <p>
                          <span class="small_text"> {{ __('messages.customers') }} 
                          </span>
                          <br>
                          {{ $res['sales_gn'][4]['sales_gn_sub']['total_customer'] }} 
                        </p>
                      </div>
                    </div>
                    @endif
                  </div>
                </a> 
                @endif
              </div>
            </nav> 
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
              <!-- first tab active -->
              <div class="tab-pane fade " id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                @if($res['sales_gn'][3]["type"]=='1')
                <section class="linemain_1_fst linemain_1_fst_cstm">
                  <div class="container">
                    <div style="display: none;" class="line_down_left_1_fst">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1_fst line_down_right_1_fst_cstm">{{ __('messages.customers') }}
                    </div>
                  </div>
                </section>
                @if(count($res['sales_gn'][3]['customers'])==0)
                <span class="linemain_1_fst linemain_1_fst_cstm">{{ __('messages.No customers') }}
                </span>
                @endif
                @if(count($res['sales_gn'][3]['customers'])>0)
                <div class="network_third_main">
                  <div class="network_third_main_sub mrgn_right network_third_main_sub_cusonly">
                  </div>
                  <div class="network_third_main_sub network_third_main_sub_cstm mrgn_left ajax_{{$res['sales_gn'][3]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5 ); $i++)
                         @if( $i 
                         < count($res['sales_gn'][3]['customers']))
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->

                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                    @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][3]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][3]['customers'])); $i++)
                      <div class="third_sub_r"> 
                      <!--   <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->


                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                    @endif




                        <p class="third_sub_name"> {{ $res['sales_gn'][3]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                </div>
                @endif
                @else
                <section class="linemain_1_fst">
                  <div class="container">
                    <div class="line_down_left_1_fst line_down_left_1_fst_sg_1">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1_fst line_down_right_1_fst_sg_1">{{ __('messages.customers') }}
                      <img src="{{asset('frontend/img/arrow_right.jpg')}}" class="nw_arw_lft">
                    </div>
                  </div>
                </section>
                <section class="linemain_2_fst">
                  <div class="container">
                    <div class="line_btm_fst">
                    </div>
                  </div>
                </section> 
                <div class="row" style="display: block;">
                  @if(!$res['sales_gn'][3]['sales_gn_sub'][0])
                  <div class="linemain_1_fst" style="width: 50%;padding-right: 0px !important;">{{ __('messages.No sales genie') }}
                  </div> 
                  @else
                  <div class="linemain_1_fst" style="width: 50%;padding-right: 0px !important;">&nbsp;
                  </div> 
                  @endif
                  @if(count($res['sales_gn'][3]['customers'])==0)   
                  <div class="linemain_1_fst" style="width: 50%;padding-right: 0% !important;padding-left: 200px!important;">{{ __('messages.No customers') }}
                  </div>
                  @else
                  <div class="linemain_1_fst" style="width: 50%;padding-right: 0% !important;padding-left: 200px!important;">&nbsp;
                  </div>
                  @endif
                </div>
                <div class="network_third_main">
                  <div class="network_third_main_sub mrgn_right">
                    @if($res['sales_gn'][3]['sales_gn_sub'][0])
                    <div class="third_sub"> 
                    <!--   <img src="{{asset('frontend/img/8.png')}}" class="mx-auto d-block"> -->

                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][3]['sales_gn_sub'][0]['first_name']." ".$res['sales_gn'][3]['sales_gn_sub'][0]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][3]['sales_gn_sub'][0]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    <div style="border:none !important" class="third_sub">
                    </div>
                    @endif
                    @if($res['sales_gn'][3]['sales_gn_sub'][1])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/7.png')}}" class="mx-auto d-block"> -->
                      @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][3]['sales_gn_sub'][1]['first_name']." ".$res['sales_gn'][3]['sales_gn_sub'][1]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][3]['sales_gn_sub'][1]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][3]['sales_gn_sub'][2])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/9.png')}}" class="mx-auto d-block"> -->
                     @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif




                      <p class="third_sub_name">{{ $res['sales_gn'][3]['sales_gn_sub'][2]['first_name']." ".$res['sales_gn'][3]['sales_gn_sub'][2]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][3]['sales_gn_sub'][2]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][3]['sales_gn_sub'][3])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/10.png')}}" class="mx-auto d-block"> -->
                          @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][3]['sales_gn_sub'][3]['first_name']." ".$res['sales_gn'][3]['sales_gn_sub'][3]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][3]['sales_gn_sub'][3]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][3]['sales_gn_sub'][4])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/10.png')}}" class="mx-auto d-block"> -->

                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif




                      <p class="third_sub_name">{{ $res['sales_gn'][3]['sales_gn_sub'][4]['first_name']." ".$res['sales_gn'][3]['sales_gn_sub'][4]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][3]['sales_gn_sub'][4]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                  </div>
                  @if(count($res['sales_gn'][3]['customers'])>0)
                  <div class="network_third_main_sub mrgn_left ajax_{{$res['sales_gn'][3]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5 ); $i++)
                         @if( $i 
                         < count($res['sales_gn'][3]['customers']))
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->

                          @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                    @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][3]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][3]['customers'])); $i++)
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][3]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                    @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][3]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                  @endif
                </div>
                @endif
                @if(count($res['sales_gn'][3]['customers'])>10)
                <a href="javascript:void(0)" id="more_btn_{{ $res['sales_gn'][3]['id'] }}" onclick="return ajax_fn({{ $res['sales_gn'][3]['id'] }});" class="more">{{ __('messages.more') }}
                </a>
                @endif
              </div>
              <!-- second tab active -->
              <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                @if($res['sales_gn'][1]["type"]=='1')   
                <section class="linemain_1_scnd linemain_1_scnd_cstm">
                  <div class="container">
                    <div style="display: none;" class="line_down_left_1_scnd">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1_scnd line_down_right_1_scnd_cstm">{{ __('messages.customers') }}
                    </div>
                  </div>
                </section>
                @if(count($res['sales_gn'][1]['customers'])==0)
                <span class="linemain_1_scnd linemain_1_scnd_cstm">{{ __('messages.No customers') }}
                </span>
                @endif
                @if(count($res['sales_gn'][1]['customers'])>0)
                <div class="network_third_main network_third_main_jq2">
                  <div class="network_third_main_sub mrgn_right">
                  </div>
                  <div class="network_third_main_sub  network_third_main_sub_jq2   mrgn_left ajax_{{$res['sales_gn'][1]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5); $i++)
                           @if( $i 
                           < count($res['sales_gn'][1]['customers']))
                      <div class="third_sub_r"> 
                      <!--   <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                    @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][1]['customers'][$i]['name'] }}
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][1]['customers'])); $i++)
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                    @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][1]['customers'][$i]['name'] }}
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                </div>
                @endif
                @else
                <section class="linemain_1_scnd">
                  <div class="container">
                    <div class="line_down_left_1_scnd line_down_left_1_scnd_jq2 ">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1_scnd line_down_right_1_scnd_jq2">{{ __('messages.customers') }}
                      <img src="{{asset('frontend/img/arrow_right.jpg')}}" class="nw_arw_lft">
                    </div>
                  </div>
                </section>
                <section class="linemain_2_scnd">
                  <div class="container">
                    <div class="line_btm_scnd">
                    </div>
                  </div>
                </section>  
                <!-- <span class="linemain_1_scnd">{{ __('messages.No customers') }}</span> -->
                <div class="row" style="display: block;">
                  @if(!$res['sales_gn'][1]['sales_gn_sub'][0])
                  <div class="linemain_1_scnd" style="width: 50%;padding-right: 0px !important;">{{ __('messages.No sales genie') }}
                  </div>
                  @else
                  <div class="linemain_1_scnd" style="width: 50%;padding-right: 0px !important;">&nbsp;
                  </div>
                  @endif
                  @if(count($res['sales_gn'][1]['customers'])==0)
                  <div class="linemain_1_scnd" style="width: 50%;padding-right: 0% !important;padding-left: 200px!important;">{{ __('messages.No customers') }}
                  </div>
                  @else
                  <div class="linemain_1" style="
                                                 width: 50%;padding-right: 0% !important;
                                                 ">&nbsp;
                  </div>
                  @endif  
                </div>
                <div class="network_third_main">
                  <div class="network_third_main_sub mrgn_right">
                    @if($res['sales_gn'][1]['sales_gn_sub'][0])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/8.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][1]['sales_gn_sub'][0]['first_name']." ".$res['sales_gn'][1]['sales_gn_sub'][0]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][1]['sales_gn_sub'][0]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    <div style="border:none !important" class="third_sub">
                    </div>
                    @endif
                    @if($res['sales_gn'][1]['sales_gn_sub'][1])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/7.png')}}" class="mx-auto d-block"> -->

                      @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][1]['sales_gn_sub'][1]['first_name']." ".$res['sales_gn'][1]['sales_gn_sub'][1]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][1]['sales_gn_sub'][1]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][1]['sales_gn_sub'][2])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/9.png')}}" class="mx-auto d-block"> -->

                     @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                    @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][1]['sales_gn_sub'][2]['first_name']." ".$res['sales_gn'][1]['sales_gn_sub'][2]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][1]['sales_gn_sub'][2]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][1]['sales_gn_sub'][3])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/10.png')}}" class="mx-auto d-block"> -->

                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][1]['sales_gn_sub'][3]['first_name']." ".$res['sales_gn'][1]['sales_gn_sub'][3]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][1]['sales_gn_sub'][3]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][1]['sales_gn_sub'][4])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/11.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][1]['sales_gn_sub'][4]['first_name']." ".$res['sales_gn'][1]['sales_gn_sub'][4]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][1]['sales_gn_sub'][4]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                  </div>
                  @if(count($res['sales_gn'][1]['customers'])>0)
                  <div class="network_third_main_sub  mrgn_left ajax_{{$res['sales_gn'][1]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5); $i++)
                           @if( $i 
                           < count($res['sales_gn'][1]['customers']))
                      <div class="third_sub_r"> 
                        <!-- <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->
                          @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][1]['customers'][$i]['name'] }}
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][1]['customers'])); $i++)
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][1]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][1]['customers'][$i]['name'] }}
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                  @endif
                </div>
                @endif
                @if(count($res['sales_gn'][1]['customers'])>10)
                <a href="javascript:void(0)" id="more_btn_{{ $res['sales_gn'][1]['id'] }}" onclick="return ajax_fn({{ $res['sales_gn'][1]['id'] }});" class="more">{{ __('messages.more') }}
                </a>
                @endif
              </div> 
              <!-- third tab active -->
              <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                @if($res['sales_gn'][0]["type"]=='1')
                <section class="linemain_1 linemain_1_third linemain_1_jq3">
                  <div class="container">
                    <div style="display: none !important;" class="line_down_left_1">
                    </div>
                    <div class="line_down_right_1 line_down_right_third">{{ __('messages.customers') }}
                    </div>
                  </div>
                </section>
                @if(count($res['sales_gn'][0]['customers'])==0)
                <span class="linemain_1 linemain_1_third linemain_1_jq3">{{ __('messages.No customers') }}
                </span>
                @endif
                @if(count($res['sales_gn'][0]['customers'])>0)
                <div class="network_third_main network_third_main_jq3">
                  <div class="network_third_main_sub mrgn_right network_third_main_sub_cusonly">
                  </div>
                  <div class="network_third_main_sub network_third_main_sub1 third_margin mrgn_left ajax_{{$res['sales_gn'][0]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5 ); $i++)
                         @if( $i 
                         < count($res['sales_gn'][0]['customers']))
                      <div class="third_sub_r"> 
                        <!-- <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->

                         @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][0]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][0]['customers'])); $i++)
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                              @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][0]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                </div>
                @endif
                @else
                <section class="linemain_1">
                  <div class="container">
                    <div class="line_down_left_1 line_down_left_1_jq3">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1 line_down_right_1_jq3">{{ __('messages.customers') }}
                      <img src="{{asset('frontend/img/arrow_right.jpg')}}" class="nw_arw_lft">
                    </div>
                  </div>
                </section>
                <section class="linemain_2">
                  <div class="container">
                    <div class="line_btm">
                    </div>
                  </div>
                </section> 
                <div class="row"  style="display: block;">
                  @if(!$res['sales_gn'][0]['sales_gn_sub'][0])
                  <div class="linemain_1" style="width: 50%;padding-right: 0px !important;">{{ __('messages.No sales genie') }}
                  </div>
                  @else
                  <div class="linemain_1" style="width: 50%;padding-right: 0px !important;">&nbsp;
                  </div>
                  @endif
                  @if(count($res['sales_gn'][0]['customers'])==0)
                  <div class="linemain_1" style="width: 50%;padding-right: 0% !important;">{{ __('messages.No customers') }}
                  </div>
                  @else
                  <div class="linemain_1" style="width: 50%;padding-right: 0% !important;">&nbsp;
                  </div>
                  @endif
                </div>
                <div class="network_third_main">
                  <div class="network_third_main_sub mrgn_right network_third_main_subcst">
                    @if($res['sales_gn'][0]['sales_gn_sub'][0])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/8.png')}}" class="mx-auto d-block"> -->

                            @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][0]['sales_gn_sub'][0]['first_name']." ".$res['sales_gn'][0]['sales_gn_sub'][0]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][0]['sales_gn_sub'][0]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    <div style="border:none !important" class="third_sub">
                    </div>
                    @endif
                    @if($res['sales_gn'][0]['sales_gn_sub'][1])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/7.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][0]['sales_gn_sub'][1]['first_name']." ".$res['sales_gn'][0]['sales_gn_sub'][1]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][0]['sales_gn_sub'][1]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][0]['sales_gn_sub'][2])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/9.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][0]['sales_gn_sub'][2]['first_name']." ".$res['sales_gn'][0]['sales_gn_sub'][2]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][0]['sales_gn_sub'][2]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][0]['sales_gn_sub'][3])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/10.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][0]['sales_gn_sub'][3]['first_name']." ".$res['sales_gn'][0]['sales_gn_sub'][3]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][0]['sales_gn_sub'][3]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][0]['sales_gn_sub'][4])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/11.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][0]['sales_gn_sub'][4]['first_name']." ".$res['sales_gn'][0]['sales_gn_sub'][4]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][0]['sales_gn_sub'][4]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                  </div>
                  @if(count($res['sales_gn'][0]['customers'])>0)
                  <div class="network_third_main_sub network_third_main_sub1 mrgn_left ajax_{{$res['sales_gn'][0]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5 ); $i++)
                         @if( $i 
                         < count($res['sales_gn'][0]['customers']))
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->
                         @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][0]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][0]['customers'])); $i++)
                      <div class="third_sub_r"> 
                      <!--   <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                           @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][0]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][0]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                  @endif
                </div>
                @endif
                @if(count($res['sales_gn'][0]['customers'])>10)
                <a href="javascript:void(0)" id="more_btn_{{ $res['sales_gn'][0]['id'] }}" onclick="return ajax_fn({{ $res['sales_gn'][0]['id'] }});" class="more">{{ __('messages.more') }}
                </a>
                @endif
              </div>
              <!-- fourth tab active -->
              <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                @if($res['sales_gn'][2]["type"]=='1')
                <section class="linemain_1_frth linemain_1_frth_cstm ">
                  <div class="container">
                    <div style="display: none" class="line_down_left_1_frth">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1_frth line_down_right_1_frth_cstm ">{{ __('messages.customers') }}
                    </div>
                  </div>
                </section>
                @if(count($res['sales_gn'][2]['customers'])==0)
                <span class="linemain_1_frth linemain_1_frth_cstm">{{ __('messages.No customers') }}
                </span>
                @endif
                @if(count($res['sales_gn'][2]['customers'])>0)
                <div class="network_third_main">
                  <div class="network_third_main_sub mrgn_right">
                  </div>
                  <!-- network_third_main_sub-->
                  <div class="network_third_main_sub mrgn_left network_third_main_sub_cst ajax_{{$res['sales_gn'][2]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5  ); $i++)
                         @if( $i 
                         < count($res['sales_gn'][2]['customers']))
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->
                                  @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][2]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][2]['customers'])); $i++)
                      <div class="third_sub_r"> 
                        <!-- <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                          @if (file_exists(public_path('upload/profile_pics/profile_pic_'.s['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.s['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][2]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                  <!-- network_third_main_sub--> 
                </div>
                @endif
                <!-- network_third_main--> 
                @else
                <!-- linemain_1_thrd-->      
                <section class="linemain_1_frth">
                  <div class="container">
                    <div class="line_down_left_1_frth">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1_frth">{{ __('messages.customers') }}
                      <img src="{{asset('frontend/img/arrow_right.jpg')}}" class="nw_arw_lft">
                    </div>
                  </div>
                </section>
                <section class="linemain_2_frth">
                  <div class="container">
                    <div class="line_btm_frth">
                    </div>
                  </div>
                </section>   
                <div class="row" style="display: block;">
                  @if(!$res['sales_gn'][2]['sales_gn_sub'][0])
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0px !important;">{{ __('messages.No sales genie') }}
                  </div> 
                  @else
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0px !important;">&nbsp;
                  </div>
                  @endif
                  @if(count($res['sales_gn'][2]['customers'])==0)
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0% !important;padding-left: 300px!important;">{{ __('messages.No customers') }}
                  </div>
                  @else
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0% !important;padding-left: 300px!important;">&nbsp;
                  </div>
                  @endif
                </div>
                <div class="network_third_main network_third_main_subcst2">
                  @if(count($res['sales_gn'][2]['sales_gn_sub']))
                  <div class="network_third_main_sub mrgn_right">
                    @if($res['sales_gn'][2]['sales_gn_sub'][0])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/8.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][2]['sales_gn_sub'][0]['first_name']." ".$res['sales_gn'][2]['sales_gn_sub'][0]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][2]['sales_gn_sub'][0]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    <div style="border:none !important" class="third_sub">
                    </div>
                    @endif
                    @if($res['sales_gn'][2]['sales_gn_sub'][1])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/7.png')}}" class="mx-auto d-block"> -->

                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][2]['sales_gn_sub'][1]['first_name']." ".$res['sales_gn'][2]['sales_gn_sub'][1]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][2]['sales_gn_sub'][1]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][2]['sales_gn_sub'][2])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/9.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][2]['sales_gn_sub'][2]['first_name']." ".$res['sales_gn'][2]['sales_gn_sub'][2]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][2]['sales_gn_sub'][2]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][2]['sales_gn_sub'][3])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/10.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif




                      <p class="third_sub_name">{{ $res['sales_gn'][2]['sales_gn_sub'][3]['first_name']." ".$res['sales_gn'][2]['sales_gn_sub'][3]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][2]['sales_gn_sub'][3]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][2]['sales_gn_sub'][4])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/10.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][2]['sales_gn_sub'][4]['first_name']." ".$res['sales_gn'][2]['sales_gn_sub'][4]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][2]['sales_gn_sub'][4]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                  </div>
                  @endif
                  @if(count($res['sales_gn'][2]['customers'])>0)
                  <!-- network_third_main_thrd-->
                  <div class="network_third_main_sub  mrgn_left  ajax_{{$res['sales_gn'][2]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5 ); $i++)
                         @if( $i 
                         < count($res['sales_gn'][2]['customers']))
                      <div class="third_sub_r"> 
                        <!-- <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->
                         @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][2]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][2]['customers'])); $i++)
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][2]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][2]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                  @endif
                  <!-- network_third_main_sub--> 
                </div>
                <!-- network_third_main--> 
                @endif
                @if(count($res['sales_gn'][2]['customers'])>10)
                <a href="javascript:void(0)" id="more_btn_{{ $res['sales_gn'][2]['id'] }}" onclick="return ajax_fn({{ $res['sales_gn'][2]['id'] }});" class="more">{{ __('messages.more') }}
                </a>
                @endif
              </div>
              <!-- fifth tab active -->
              <div class="tab-pane fade" id="nav-aboutnew" role="tabpanel" aria-labelledby="nav-aboutnew-tab">
                @if($res['sales_gn'][4]["type"]=='1')
                <section class="linemain_1_ffth">
                  <div class="container">
                    <div class="line_down_left_1_ffth" style="display: none;">
                    </div>
                    <div class="line_down_right_1_ffth line_down_right_1_ffth_custom">{{ __('messages.customers') }}
                    </div>
                  </div>
                </section>
                @if(count($res['sales_gn'][4]['customers'])==0)
                <span class="linemain_1_frth linemain_1_frth_cstm">{{ __('messages.No customers') }}
                </span>
                @endif
                @if(count($res['sales_gn'][4]['customers'])>0)
                <div class="network_third_main">
                  <div class="network_third_main_sub mrgn_right network_third_main_sub_cusonly">
                  </div>
                  <div class="network_third_main_sub mrgn_left third_div ajax_{{$res['sales_gn'][4]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5); $i++)
                           @if( $i 
                           < count($res['sales_gn'][4]['customers']))
                      <div class="third_sub_r"> 
                        <!-- <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][4]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][4]['customers'])); $i++)
                      <div class="third_sub_r"> 
                        <!-- <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                              @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][4]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                </div>
                @endif
                @else
                <section class="linemain_1_ffth">
                  <div class="container">
                    <div class="line_down_left_1_ffth">
                      <img src="{{asset('frontend/img/arrow_left.jpg')}}" class="nw_arw_rght"> {{ __('messages.Sales Genies') }}
                    </div>
                    <div class="line_down_right_1_ffth">{{ __('messages.customers') }}
                      <img src="{{asset('frontend/img/arrow_right.jpg')}}" class="nw_arw_lft">
                    </div>
                  </div>
                </section>
                <section class="linemain_2_ffth">
                  <div class="container">
                    <div class="line_btm_ffth">
                    </div>
                  </div>
                </section>  
                <!-- <span class="linemain_1_frth">{{ __('messages.No customers') }}</span> -->
                <div class="row" style="display: block;">
                  @if(!$res['sales_gn'][4]['sales_gn_sub'][0])
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0px !important;">{{ __('messages.No sales genie') }}
                  </div> 
                  @else
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0px !important;">&nbsp;
                  </div> 
                  @endif
                  @if(count($res['sales_gn'][0]['customers'])==0)
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0% !important;padding-left: 380px!important;">{{ __('messages.No customers') }}
                  </div>
                  @else
                  <div class="linemain_1_frth" style="width: 50%;padding-right: 0% !important;padding-left: 380px!important;">&nbsp;
                  </div>
                  @endif
                </div>
                <div class="network_third_main network_third_main_subcst3">
                  <div class="network_third_main_sub mrgn_right">
                    @if($res['sales_gn'][4]['sales_gn_sub'][0])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/8.png')}}" class="mx-auto d-block"> -->
                               @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][0]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif




                      <p class="third_sub_name">{{ $res['sales_gn'][4]['sales_gn_sub'][0]['first_name']." ".$res['sales_gn'][4]['sales_gn_sub'][0]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][4]['sales_gn_sub'][0]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    <div style="border:none !important" class="third_sub">
                    </div>
                    @endif
                    @if($res['sales_gn'][4]['sales_gn_sub'][1])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/7.png')}}" class="mx-auto d-block"> -->
                                 @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][1]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif



                      <p class="third_sub_name">{{ $res['sales_gn'][4]['sales_gn_sub'][1]['first_name']." ".$res['sales_gn'][4]['sales_gn_sub'][1]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][4]['sales_gn_sub'][1]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][4]['sales_gn_sub'][2])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/9.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][2]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][4]['sales_gn_sub'][2]['first_name']." ".$res['sales_gn'][4]['sales_gn_sub'][2]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][4]['sales_gn_sub'][2]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][4]['sales_gn_sub'][3])
                    <div class="third_sub"> 
                     <!--  <img src="{{asset('frontend/img/10.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][3]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][4]['sales_gn_sub'][3]['first_name']." ".$res['sales_gn'][4]['sales_gn_sub'][3]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][4]['sales_gn_sub'][3]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                    @if($res['sales_gn'][4]['sales_gn_sub'][4])
                    <div class="third_sub"> 
                      <!-- <img src="{{asset('frontend/img/11.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['sales_gn_sub'][4]['fk_frontend_user_id'].'.png')}}" class="pro_pic third_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar2.png')}}" class="pro_pic third_lvl img-fluid">
                     @endif


                      <p class="third_sub_name">{{ $res['sales_gn'][4]['sales_gn_sub'][4]['first_name']." ".$res['sales_gn'][4]['sales_gn_sub'][4]['last_name'] }}
                      </p>
                      <p class="third_sub_customer">
                        <span class="small_text"> {{ __('messages.customers') }} 
                        </span>
                        <br>
                        {{ $res['sales_gn'][4]['sales_gn_sub'][4]['sales_gn_3_count'] }} 
                      </p>
                    </div>
                    @else
                    @endif
                  </div>


                  @if(count($res['sales_gn'][4]['customers'])>0)
                  <div class="network_third_main_sub mrgn_left ajax_{{$res['sales_gn'][4]['id']}}">
                    <div class="w-100">
                      @for ($i = 0; ($i 
                      < 5); $i++)
                           @if( $i 
                           < count($res['sales_gn'][4]['customers']))
                      <div class="third_sub_r"> 
                        <!-- <img src="{{asset('frontend/img/12.png')}}" class="mx-auto d-block"> -->
                        @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif



                        <p class="third_sub_name"> {{ $res['sales_gn'][4]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @else
                      <div class="third_sub_r"> 
                      </div>
                      @endif
                      @endfor
                    </div>
                    <div class="w-100 mt">
                      @for ($i; ($i 
                      < 10  && $i 
                          < count($res['sales_gn'][4]['customers'])); $i++)
                      <div class="third_sub_r"> 
                       <!--  <img src="{{asset('frontend/img/17.png')}}" class="mx-auto d-block"> -->
                       @if (file_exists(public_path('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')))
                      <img src="{{asset('upload/profile_pics/profile_pic_'.$res['sales_gn'][4]['customers'][$i]['fk_frontend_user_id'].'.png')}}" class="pro_pic customer_lvl img-fluid">
                          @else 
                      <img src="{{asset('frontend/img/avathar4.png')}}" class="pro_pic customer_lvl img-fluid">
                     @endif


                        <p class="third_sub_name"> {{ $res['sales_gn'][4]['customers'][$i]['name'] }} 
                        </p>
                      </div>
                      @endfor
                    </div>
                  </div>
                  @endif
                </div>
                @endif
                @if(count($res['sales_gn'][4]['customers'])>10)
                <a href="javascript:void(0)" id="more_btn_{{ $res['sales_gn'][4]['id'] }}" onclick="return ajax_fn({{ $res['sales_gn'][4]['id'] }});" class="more">{{ __('messages.more') }}
                </a>
                @endif
              </div>
            </div>
            <!-- <a href="javascript:void(0)" class="more">more</a> -->
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
@endsection
@section('javascript')
<script>

  $(document).ready(function(){
    var outerContent = $('.table-responsive');
    var innerContent = $('.table-responsive > div');
    outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2);        
  });



  function ajax_fn(id){
    var url =  '{{ url('ajax_network') }}';
    $.ajax({
      type:'post',
      url:url,
      data:{
        "_token": "{{ csrf_token() }}", "id":id}
      ,
      success: function(data){
        $(".ajax_"+id).append(data);
        $("#more_btn_"+id).hide();
      }
    }
          );
  }
  $('.network_third_main_subcst').each(
    function() {
      var child=$('.third_sub', $(this)).length;
      $(".network_third_main_subcst").css( {
        paddingLeft : getFromCount(child) }
                                         );
    }
  );
  $('.network_third_main_subcst2').each(
    function() {
      var child=$('.third_sub', $(this)).length;
      $(".network_third_main_subcst2").css( {
        paddingLeft : getFromCount(child) }
                                          );
    }
  );
  $('.network_third_main_subcst3').each(
    function() {
      var child=$('.third_sub', $(this)).length;
      $(".network_third_main_subcst3").css( {
        paddingLeft : getFromCount(child) }
                                          );
    }
  );
  $('.cus-link').each(
    function() {
      var child=$('.nav-link', $(this)).length;
      $(".cus-link").css( {
        paddingLeft : getFromsalesgen(child) , paddingRight : getFromsalesgen(child)}
                        );
    }
  );
  function getFromCount(child) {
    if (child == 1) {
      return '223px';
    }
    else if (child == 2) {
      return '168px';
    }
    else if (child == 3) {
      return '113px';
    }
    else if (child == 4) {
      return '59px';
    }
  }
  function getFromsalesgen(child) {
    if (child == 2) {
      $('.line_down_left_1_scnd_jq2').attr('style', 'width: 47% !important');
      $('.line_down_right_1_scnd_jq2').attr('style', 'width: 53% !important');
      $('.linemain_1_scnd_cstm').attr('style', 'padding-left: 39% !important');
      $('.line_down_left_1_jq3').attr('style', 'width: 70% !important');
      $('.line_down_right_1_jq3').attr('style', 'width: 30% !important');
      //
      $('.network_third_main_sub_jq2').attr('style', 'margin-left: 170px !important');
      $('.linemain_1_jq3').attr('style', 'padding-left: 59% !important');
      $('.network_third_main_jq3').attr('style', 'margin-left: 102px !important');
      //$('.linemain_1_scnd').attr('style', 'padding-left: 4% !important');
      //$('.line_down_left_1_scnd').attr('style', 'width: 49% !important');
      //$('.line_down_right_1_scnd').attr('style', 'width: 51% !important');
      return '30%';
    }
    else if (child == 3) {
      return '15%';
    }
    else if (child == 1) {
      $('.line_down_left_1_jq3').attr('style', 'width: 50% !important');
      //       line_down_right_1
      // $('.line_down_right_1_jq').attr('style', 'width: 52% !important');
      return '40%';
    }
    else if (child == 4) {
      //      $('.line_down_left_1_jq3').attr('style', 'width: 50% !important');
      //       line_down_right_1
      // $('.line_down_right_1_jq').attr('style', 'width: 52% !important');
      $('.line_down_left_1_jq3').attr('style', 'width: 67% !important');
      $('.line_down_right_1_jq3').attr('style', 'width: 33% !important');
      //linemain_1_jq3
      $('.linemain_1_jq3').attr('style', 'padding-left: 57% !important');
      $('.network_third_main_jq3').attr('style', 'margin-left: 124px !important');
      $('.line_down_left_1_scnd_jq2').attr('style', 'width: 50% !important');
      $('.line_down_right_1_scnd_jq2').attr('style', 'width: 50% !important');
      $('.linemain_1_scnd_cstm').attr('style', 'padding-left: 40% !important');
      $('.network_third_main_jq2').attr('style', 'margin-left: 100px !important');
      $('.line_down_left_1_fst_sg_1').attr('style', 'width: 30% !important');
      $('.line_down_right_1_fst_sg_1').attr('style', 'width: 70% !important');
      // 
      $('.linemain_1_fst_cstm').attr('style', 'padding-left: 23% !important');
      $('.linemain_1_frth_cstm').attr('style', 'padding-left: 74% !important');
      return '15%';
    }
  }
  $(function() {
    var $tabButtonItem = $('#tab-button li'),
        $tabSelect = $('#tab-select'),
        $tabContents = $('.tab-contents'),
        activeClass = 'is-active';
    $tabButtonItem.first().addClass(activeClass);
    $tabContents.not(':first').hide();
    $tabButtonItem.find('a').on('click', function(e) {
      var target = $(this).attr('href');
      $tabButtonItem.removeClass(activeClass);
      $(this).parent().addClass(activeClass);
      $tabSelect.val(target);
      $tabContents.hide();
      $(target).show();
      e.preventDefault();
    }
                               );
    $tabSelect.on('change', function() {
      var target = $(this).val(),
          targetSelectNum = $(this).prop('selectedIndex');
      $tabButtonItem.removeClass(activeClass);
      $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
      $tabContents.hide();
      $(target).show();
    }
                 );
  }
   );
</script>
@endsection
