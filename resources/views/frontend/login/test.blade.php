
@extends('frontend.frontend')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>



<section class="welcome">

 <!-- Trigger/Open The Modal -->
 <div id="preview-crop-image" style="background:#9d9d9d;width:300px;padding:50px 50px;height:300px;"></div>

      </div>
<button id="myBtn">change pic</button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    
	
	<div class="container">

<div class="panel panel-info">

  <div class="col-md-4" style="padding:5%;">

      <strong>Select image to crop:</strong>

      <input type="file" id="image">


      

      </div>

  <div class="panel-body">


  


    <div class="row">

      <div class="col-md-4 text-center">

      <div id="upload-demo"></div>
	  
	  <button class="btn btn-primary btn-block upload-image" style="margin-top:-25px">Upload Image</button>

      </div>

      


      <!--<div class="col-md-4">

      <div id="preview-crop-image" style="background:#9d9d9d;width:300px;padding:50px 50px;height:300px;"></div>

      </div>-->

    </div>


  </div>

</div>

</div>
	
	
  </div>

</div> 

  



<script type="text/javascript">


$.ajaxSetup({

headers: {

  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

});


var resize = $('#upload-demo').croppie({

  enableExif: true,

  enableOrientation: true,    

  viewport: { 

      width: 200,

      height: 200,

      type: 'circle'

  },

  boundary: {

      width: 300,

      height: 300

  }

});


$('#image').on('change', function () { 

var reader = new FileReader();

  reader.onload = function (e) {

    resize.croppie('bind',{

      url: e.target.result

    }).then(function(){

      console.log('jQuery bind complete');

    });

  }

  reader.readAsDataURL(this.files[0]);

});


$('.upload-image').on('click', function (ev) {

resize.croppie('result', {

  type: 'canvas',

  size: 'viewport'

}).then(function (img) {

  $.ajax({

    url: "{{route('upload.image')}}",

    type: "POST",

    data: {"image":img},

    success: function (data) {

      html = '<img src="' + img + '" />';

      $("#preview-crop-image").html(html);

    }

  });

});

});


</script>

</section>

<style>
 /* The Modal (background) */
 .modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 50%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
} 
</style>


@endsection


@section('javascript')

<script>

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
} 

</script>

@endsection