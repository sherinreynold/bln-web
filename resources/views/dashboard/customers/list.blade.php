@extends('dashboard.base')

@section('content')




        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Customers') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('customers.create') }}" class="btn btn-primary m-2">{{ __('Add Customer') }}</a>
                        </div>
                        
						
						<div class="row mb-3">
                    <div class="col-sm-8">
                        <form action="{{ url('admin/customers') }}" methos="GET">
						<table style="width: 50%;margin-left: -8px;">
						<tr>
                            <td>
							<input type="text" name="code" class="form-control" placeholder="Customer Code" value="{{@$_GET['code']}}">
							
                           </td>
						   <td>
							<input type="text" name="reff_code" class="form-control" placeholder="Referred By" value="{{@$_GET['reff_code']}}">
							
                           </td>
						   <td>
                            <button type="submit" class="btn btn-secondary">Filter</button>
							</td>
							</tr>
							</table>
							
                        </form>
                    </div>
                </div>
						
						
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
							<th>Mobile</th>
							<th>Referred By</th>
              <th>Invitation Status</th>            
							<th>Account Status</th>
							<th></th>
							<th></th>
							<th></th>
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($notes as $note)
                            <tr>
                              <td><strong>{{ $note->code }}</strong></td>
                              <td><strong>{{ $note->name }}</strong></td>
                              <td>{{ $note->email }}</td>
							  <td>{{ $note->phone }}</td>
							  <td>{{ $note->parent_code }}</td>
                <td>@if($note->customer_status == '1') Invitation Sent  @endif
                @if($note->customer_status == '2') Invitation accepted  @endif
                @if($note->customer_status == '3') Profile Verified  @endif
                 </td>
                              
                              
							  
							  <td>
                 
                @if($note->customer_status == '3')
                <div class="form-group row">
              <select class="form-control" id="account_status" onchange="return change_account_status(this,{{$note->id}})">
							
							<option value='enabled' @if($note->account_status == 'enabled') selected  @endif >Enabled</option>
							<option value='blacklisted' @if($note->account_status == 'blacklisted') selected  @endif >Blacklisted</option>
              <option value='terminated' @if($note->account_status == 'terminated') selected  @endif >Terminated</option>
							</select>
							</div>
              @endif
                              </td>
                              <td>
                                <!--<a href="{{ url('admin/red_genie/' . $note->id) }}" class="btn btn-block btn-primary">View</a>-->
                              </td>
                              <td>
                                <!--<a href="{{ url('admin/red_genie/' . $note->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>-->
                              </td>
                              <td>
                                <!--<form action="{{ route('red_genie.destroy', $note->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-block btn-danger">Delete</button>
                                </form>-->
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     {{ $notes->appends($_GET)->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')
<script>
function change_account_status(ele,id){
  var val = $(ele).val();
 
  $.ajax({

url:'{{url("admin/account_status/change")}}',
data:'id='+id+'&acc_status='+val,
success:function(){
 alert('Account status changed to '+val+'  successfully.');
}
});

}
</script>
@endsection

