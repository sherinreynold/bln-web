@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Create Customer Registration Link') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('customers.store')}}">
                            @csrf

                            <div class="form-group row ">
                                <label>Parent</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="parent" id="rg_drp" required  autofocus>
							<option value=''>--Select Parent--</option>
							<?php if($parents)
							{
								foreach($parents as $parent)
								{
								?>
							<option value="{{ $parent['id'] }}"> {{ $parent['code'] }}-{{$parent['name']}}</option>
							<?php } } ?>
							
							</select>
							</div>
							
							<div class="form-group row">
                                <label>Type</label>
                                <select name="type" class="form-control" required autofocus>
								<option value=''>Select</option>
								<option value='3'>Customer</option>
								<option value='2'>Vendor</option>
								</select>
                            </div>
							
                            <div class="form-group row">
                                <label>Name</label>
                                <input class="form-control" type="text" placeholder="{{ __('Name') }}" name="name" required autofocus>
                            </div>
							
                           							
							<div class="form-group row">
                                <label>Email Id</label>
                                <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>
                            </div>
							
							
							
							<div class="form-group row">
                                <label>Mobile</label>
                                <input class="form-control" type="mobile" placeholder="{{ __('Mobile') }}" name="mobile" required autofocus onkeypress="return isNmb(event)">
                            </div>
							
							

                           
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('customers.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection