@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify">invoice no:</i> {{ @Request::segment(3) }}</br>
                      @if($credits[0]->refferal_code != 'admin' && $credits[0]->type != '3'  )
					  <i class="fa fa-align-justify">invoice amount:</i> {{ @$credits[0]->invoice_amount }}</br>
					  <i class="fa fa-align-justify">invoice reff:</i> {{ @$credits[0]->invoice_reff_id }}</br>
					  <i class="fa fa-align-justify">Customer code:</i> {{ @$credits[0]->refferal_code }}
            @endif

            @if($credits[0]->refferal_code == 'admin' )
					  <i class="fa fa-align-justify">Description:</i> {{ @$credits[0]->description }}</br>
					 
            @endif
					  
					  </div>
                    <div class="card-body">
					
					<table style="width:100%;margin-bottom:25px;">
		<tr><td><h4>@if($credits[0]->type == '3') Debited From  @else Credited to @endif</h4></td><td><h4> Points</h4></td><td><h4> Date</h4></td></tr>	
		<tr><td><h4></h4></td><td><h4></h4></td><td><h4></h4></td></tr>
		
		@foreach($credits as $credit)
		<tr>
		<td>{{ @$credit->code }} - {{ @$credit->name }}</td>
		<td> {{ @$credit->point }}</td>
		<td> {{ @$credit->created_at }}</td>
		</tr>
		@endforeach
					
					</table>
                       
                        
                        <a href="{{ route('points.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection