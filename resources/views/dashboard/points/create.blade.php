@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Add Points') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('points.store') }}">
                            @csrf
                            

                          
                            <!--<div class="form-group row">
                                <input type="radio" name="parent" value='r' checked>Redgenie&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="parent" value='s' >Salesgenie

                                

                                </div>-->

							
							<div class="form-group row rg">
                                <label>User</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="user_id" id="rg_drp" required  autofocus>
							<option value=''>--Select User--</option>
							<?php if($users)
							{
								foreach($users as $user)
								{
								?>
							<option value="{{ $user['id'] }}">{{ $user['name'] }} - {{ $user['code'] }}</option>
							<?php } } ?>
							
							</select>
							</div>

              <div class="form-group row">
                                <label>Description</label>
                                <textarea class="form-control" type="text" placeholder="{{ __('Point') }}" name="description" require > </textarea>
                            </div>
							
							<div class="form-group row">
                                <label>Point</label>
                                <input class="form-control" type="text" placeholder="{{ __('Point') }}" name="point" required autofocus value="{{ $note->point }}" onkeypress="return isNmb(event)">
                            </div>


							
							

                           
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('points.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')
<script>
$('input[type=radio][name=parent]').change(function() {

    var val=$(this).val();
    if(val=='s'){
        $(".sg_plus").show();
        $('#sg_drp').prop('required',true);
        $('#rg_drp').removeAttr('required');
        $(".rg").hide();
      
    }else{
        $(".sg_plus").hide();
        $('#sg_drp').removeAttr('required');
        $('#rg_drp').prop('required',true);
        $(".rg").show();
        
    }

});


</script>
@endsection