@extends('dashboard.base')

@section('content')




        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Red Genie') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('red_genie.create') }}" class="btn btn-primary m-2">{{ __('Add Red Genie') }}</a>
                        </div>
                        
						
						<div class="row mb-3">
                    <div class="col-sm-8">
                        <form action="{{ url('admin/red_genie') }}" methos="GET">
						<table style="width: 50%;margin-left: -8px;">
						<tr>
                            <td>
							<input type="text" name="code" class="form-control" placeholder="Code" value="{{@$_GET['code']}}">
							
                           </td>
						   <td>
                            <button type="submit" class="btn btn-secondary">Filter</button>
							</td>
							</tr>
							</table>
							
                        </form>
                    </div>
                </div>
						
						
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
							<th>Mobile</th>
                            
							<th>Account Status</th>
							<th></th>
							<th></th>
							<th></th>
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($notes as $note)
                            <tr>
                              <td><strong>{{ $note->code }}</strong></td>
                              <td><strong>{{ $note->name }}</strong></td>
                              <td>{{ $note->email }}</td>
							  <td>{{ $note->phone }}</td>
                              
                              
							  
							  <td>


                <div class="form-group row">
              <select class="form-control" id="account_status" onchange="return change_account_status(this,{{$note->id}})">
							
							<option value='enabled' @if($note->account_status == 'enabled') selected  @endif >Enabled</option>
							<option value='blacklisted' @if($note->account_status == 'blacklisted') selected  @endif >Blacklisted</option>
              <option value='terminated' @if($note->account_status == 'terminated') selected  @endif >Terminated</option>
							</select>
							</div>

                                <!--<a href="{{ url('/red_genie/reg_links/' . $note->id) }}" class="btn btn-block btn-primary">Reg Links</a>-->
                              </td>
                              <td>
                                <a href="{{ url('admin/red_genie/' . $note->id) }}" class="btn btn-block btn-primary">View</a>
                              </td>
                              <td>
                                <a href="{{ url('admin/red_genie/' . $note->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>
                              </td>
                              <td>
                                <form action="{{ route('red_genie.destroy', $note->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-block btn-danger">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     {{ $notes->appends($_GET)->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')
<script>
function change_account_status(ele,id){
  var val = $(ele).val();
 
  $.ajax({

url:'{{url("admin/account_status/change")}}',
data:'id='+id+'&acc_status='+val,
success:function(){
 alert('Account status changed to '+val+'  successfully.');
}
});

}
</script>
@endsection

