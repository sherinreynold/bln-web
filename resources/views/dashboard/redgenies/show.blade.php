@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> Red Genie : {{ $note->first_name }}</div>
                    <div class="card-body">
					
					<table style="width:100%;margin-bottom:25px;">
		<tr><td><h4>Code</h4></td><td><h4>: {{ $note->code }}</h4></td></tr>	
		<tr><td><h4>Name</h4></td><td><h4>: {{ $note->name }}</h4></td></tr>
		<!--<tr><td><h4>Last Name</h4></td><td><h4>: {{ $note->last_name }}</h4></td></tr>-->
		<tr><td><h4>Email</h4></td><td><h4>: {{ $note->email }}</h4></td></tr>
		<tr><td><h4>Mobile</h4></td><td><h4>: {{ $note->phone }}</h4></td></tr>
		<tr><td><h4>Address</h4></td><td><h4>: {{ $note->address }}</h4></td></tr>
		<!--<tr><td><h4>City</h4></td><td><h4>: {{ $note->city }}</h4></td></tr>
		<tr><td><h4>State</h4></td><td><h4>: {{ $note->state }}</h4></td></tr>
		<tr><td><h4>Zip</h4></td><td><h4>: {{ $note->zip }}</h4></td></tr>-->
					
					</table>
                       
                        
                        <a href="{{ route('red_genie.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection