@extends('dashboard.base')

@section('content')




        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Root Node') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <!--<a href="{{ route('red_genie.create') }}" class="btn btn-primary m-2">{{ __('Add Red Genie') }}</a>-->
                        </div>
                        
						
				
						
						
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                           <!-- <th>Code</th>-->
                            <th>Name</th>
                            <th>Email</th>
							<th>Mobile</th>
                            
							<th></th>
							<th></th>
							<th></th>
							<th></th>
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($notes as $note)
                            <tr>
                              <!--<td><strong>{{ $note->code }}</strong></td>-->
                              <td><strong>{{ $note->name }}</strong></td>
                              <td>{{ $note->email }}</td>
							  <td>{{ $note->phone }}</td>
                              
                              
							  
							  <td>
                                <!--<a href="{{ url('/red_genie/reg_links/' . $note->id) }}" class="btn btn-block btn-primary">Reg Links</a>-->
                              </td>
                              <td>
                                
                              </td>
                              <td>
                               <!-- <a href="{{ url('admin/red_genie/' . $note->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>
                              -->
                              </td>
                              <td>
                                
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     {{ $notes->appends($_GET)->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

