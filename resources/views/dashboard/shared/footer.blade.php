<footer class="c-footer">
<div class="container">
      <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6"><p class="ftr_left">©2020 Redlogik - All Rights Reserved</p> 
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6"><p class="ftr_right">Terms of Use | Privacy Policy</p> 
      </div>
    </div>
  </div>
</footer>