-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 11, 2020 at 06:53 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bln`
--

-- --------------------------------------------------------

--
-- Table structure for table `bln_account_type`
--

CREATE TABLE `bln_account_type` (
  `id` int(11) NOT NULL,
  `type` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bln_account_type`
--

INSERT INTO `bln_account_type` (`id`, `type`) VALUES
(1, 'FM'),
(2, 'EMP-O');

-- --------------------------------------------------------

--
-- Table structure for table `bln_redumption_request`
--

CREATE TABLE `bln_redumption_request` (
  `id` int(11) NOT NULL,
  `fk_frontend_user_id` int(11) NOT NULL,
  `amount` decimal(7,2) NOT NULL,
  `status` enum('pending','approved','rejected') NOT NULL DEFAULT 'pending',
  `reason` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `deleted_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bln_redumption_request`
--

INSERT INTO `bln_redumption_request` (`id`, `fk_frontend_user_id`, `amount`, `status`, `reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 20, '100.00', 'approved', NULL, '2020-08-09 18:30:00', '2020-08-10 08:24:44', '2020-08-09 18:30:00'),
(2, 21, '250.00', 'pending', 'test250', '2020-08-09 18:30:00', '2020-08-10 08:20:54', '2020-08-09 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `bln_roles`
--

CREATE TABLE `bln_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bln_roles`
--

INSERT INTO `bln_roles` (`id`, `role`) VALUES
(1, 'RG'),
(2, 'SG+'),
(3, 'SG'),
(4, 'CUSTOMER');

-- --------------------------------------------------------

--
-- Table structure for table `bln_statements`
--

CREATE TABLE `bln_statements` (
  `id` int(11) NOT NULL,
  `fk_frontend_user_id` int(11) NOT NULL,
  `refferal_code` varchar(150) NOT NULL,
  `rl_customer_id` int(11) NOT NULL,
  `type` enum('1','2','3') NOT NULL DEFAULT '2',
  `invoice_amount` decimal(9,2) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `invoice_no` varchar(150) DEFAULT NULL,
  `invoice_reff_id` int(11) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `point` decimal(7,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bln_statements`
--

INSERT INTO `bln_statements` (`id`, `fk_frontend_user_id`, `refferal_code`, `rl_customer_id`, `type`, `invoice_amount`, `currency`, `invoice_no`, `invoice_reff_id`, `description`, `point`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 16, 'BLN00000017', 1, '1', NULL, NULL, NULL, NULL, NULL, '100.00', '2020-08-07 06:47:53', '2020-08-07 06:47:53', NULL),
(3, 13, 'BLN00000017', 1, '1', NULL, NULL, NULL, NULL, NULL, '100.00', '2020-08-07 06:47:53', '2020-08-07 06:47:53', NULL),
(10, 22, 'C0000000023', 1, '2', '10000.00', 'INR', '1235', 159, NULL, '600.00', '2020-08-08 02:09:59', '2020-08-08 02:09:59', NULL),
(11, 21, 'C0000000023', 1, '2', '10000.00', 'INR', '1235', 159, NULL, '250.00', '2020-08-08 02:09:59', '2020-08-08 02:09:59', NULL),
(12, 20, 'C0000000023', 1, '2', '10000.00', 'INR', '1235', 159, NULL, '150.00', '2020-08-08 02:09:59', '2020-08-08 02:09:59', NULL),
(18, 20, 'admin', 0, '2', NULL, NULL, 'adm18', NULL, 'test points added by admin', '500.00', '2020-08-10 02:51:01', '2020-08-10 02:51:01', NULL),
(20, 20, '1', 0, '3', NULL, NULL, 'REDUM_1', NULL, 'redumption request', '100.00', '2020-08-10 08:24:44', '2020-08-10 08:24:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `id` int(11) NOT NULL,
  `fk_frontend_user_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`id`, `fk_frontend_user_id`, `title`, `description`) VALUES
(7, 14, 'exp1', 'sdfds fhgfhgf'),
(8, 2, 'xc', 'xcx');

-- --------------------------------------------------------

--
-- Table structure for table `folder`
--

CREATE TABLE `folder` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_id` int(10) UNSIGNED DEFAULT NULL,
  `resource` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `folder`
--

INSERT INTO `folder` (`id`, `created_at`, `updated_at`, `name`, `folder_id`, `resource`) VALUES
(1, NULL, NULL, 'root', NULL, NULL),
(2, NULL, NULL, 'resource', 1, 1),
(3, NULL, NULL, 'documents', 1, NULL),
(4, NULL, NULL, 'graphics', 1, NULL),
(5, NULL, NULL, 'other', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `frontend_users`
--

CREATE TABLE `frontend_users` (
  `id` int(11) NOT NULL,
  `code` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `bln_role_id` enum('1','2','3','4','0') NOT NULL DEFAULT '1',
  `bln_acc_type_id` int(11) DEFAULT NULL,
  `port_cluster_id` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `country` varchar(200) DEFAULT NULL,
  `region` varchar(150) DEFAULT NULL,
  `currency` varchar(150) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `password` text,
  `customer_type` enum('0','1','2') NOT NULL DEFAULT '0',
  `rl_customer_id` int(11) DEFAULT NULL,
  `customer_status` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1:link sent, 2: invitation accepted,  3:profile verified',
  `account_status` enum('enabled','blacklisted','terminated','deleted') NOT NULL DEFAULT 'enabled',
  `api_token` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frontend_users`
--

INSERT INTO `frontend_users` (`id`, `code`, `name`, `address`, `username`, `email`, `phone`, `bln_role_id`, `bln_acc_type_id`, `port_cluster_id`, `country`, `region`, `currency`, `parent_id`, `password`, `customer_type`, `rl_customer_id`, `customer_status`, `account_status`, `api_token`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BLN00000001', 'Anoop', NULL, NULL, 'testRootNode@gmail.com', '1234567890', '0', 1, '1', NULL, NULL, NULL, 0, NULL, '0', NULL, '1', 'enabled', NULL, '', NULL, NULL, NULL),
(5, 'BLN00000005', 'rg01', 'adressaa', 'r1', 'rg1@gmail.com', '123', '1', 2, '1', NULL, NULL, NULL, 1, '$2y$10$V0vXFfTyBWolA9/qzjOtPepkCXPZPdwqco9lQRACxZM9Yk1Z5N23e', '0', NULL, '1', 'enabled', NULL, '', '2020-08-06 03:36:40', '2020-08-06 03:52:22', NULL),
(13, 'BLN00000013', 'sgp1', 'ijo', 'sp1', 'sgp1@gmail.com', '111', '2', NULL, '1', NULL, NULL, NULL, 5, '$2y$10$so3BxJcsUgymavyjVMDSleUN.VXlvfIH5sUUmvdPURIBY.2Oc/k0O', '0', NULL, '1', 'enabled', NULL, '', '2020-08-06 06:58:10', '2020-08-06 06:58:10', NULL),
(14, 'BLN00000014', 'sg1', 'yry', 's1', 'sg1@gmail.com', '874596784', '3', NULL, '1', NULL, NULL, NULL, 13, '$2y$10$19hn7NGTDvUSRh.sk67sveoNsMnoU9ErbMM35q1zqnWR4BIC.IafW', '0', NULL, '1', 'enabled', NULL, '', '2020-08-06 07:00:03', '2020-08-06 08:45:10', NULL),
(15, 'BLN00000015', 'rg sherin', 'gfd', 'sherin', 'sherin.taurus@gmail.com', '898', '1', NULL, '1', NULL, NULL, NULL, 1, '$2y$10$sm6iU09P7DYDciCRETYXI.NOq0LPkXCVM4iDp9J6JFMmngcEj8sEm', '0', NULL, '1', 'enabled', NULL, '', '2020-08-07 01:19:49', '2020-08-07 01:19:49', NULL),
(16, 'BLN00000016', 'Sales Genie 1', 'ghfg', 'sherin.taurus+1@gmail.com', 'sherin.taurus+1@gmail.com', '45646', '3', NULL, '1', NULL, NULL, NULL, 13, '$2y$10$X6F/J.tlK6DMAox9TR.mquprRXAhTmja/LAtibC70ygx4gojjo5fu', '0', NULL, '1', 'enabled', NULL, '', '2020-08-07 01:44:12', '2020-08-07 01:44:12', NULL),
(17, 'BLN00000017', 'CUSTOMER 1', 'ADDRESS', 'c1', 'c1@gmail.com', '45454', '4', NULL, '1', NULL, NULL, NULL, 16, '$2y$10$X6F/J.tlK6DMAox9TR.mquprRXAhTmja/LAtibC70ygx4gojjo5fu', '0', 1, '3', 'enabled', NULL, '', NULL, '2020-08-07 06:47:53', NULL),
(18, 'C0000000018', 'customer2', NULL, NULL, 'sherin.taurus+2@gmail.com', '255445', '4', NULL, '1', NULL, NULL, NULL, 15, NULL, '2', NULL, '1', 'enabled', NULL, '', '2020-08-07 04:46:13', '2020-08-07 04:46:14', NULL),
(19, 'C0000000019', 'customer3', NULL, NULL, 'sherin.taurus+3@gmail.com', '9999', '4', NULL, '1', NULL, NULL, NULL, 13, NULL, '2', NULL, '1', 'enabled', NULL, '', '2020-08-07 04:48:58', '2020-08-07 04:48:59', NULL),
(20, 'BLN00000020', 'Reuben', 'fghfdd', 'telen', 'sherin.taurus+5@gmail.com', '456', '1', NULL, '1', NULL, NULL, NULL, 1, '$2y$10$9siRbYkWuTRVqt445TGbres77Jzl7g42kU2EnB/sMzJkAiYAVN9a6', '0', NULL, '1', 'enabled', NULL, '', '2020-08-08 02:04:31', '2020-08-08 02:05:47', NULL),
(21, 'BLN00000021', 'Telen', 'gfdsg', 'telen1', 'sherin.taurus+6@gmail.com', '9889989', '2', NULL, '1', NULL, NULL, NULL, 20, '$2y$10$i9.M4xySjRkM.OTwxh6jTepLqQy10qgFZptU9l6wRZWrRzYsLguiS', '0', NULL, '1', 'enabled', NULL, '', '2020-08-08 02:07:33', '2020-08-08 02:07:33', NULL),
(22, 'BLN00000022', 'sherin', 'zx', 'sher', 'sherin.taurus+7@gmail.com', '255445', '3', NULL, '1', NULL, NULL, NULL, 21, '$2y$10$vANCvl4iWewf7PPMzTLWru9pm8X6nDfd0EIpduFnV.Z8g9gq9W2w.', '0', NULL, '1', 'enabled', NULL, '', '2020-08-08 02:08:43', '2020-08-08 02:08:44', NULL),
(23, 'C0000000023', 'grace', NULL, NULL, 'sherin.taurus+8@gmail.com', '656', '4', NULL, '1', NULL, NULL, NULL, 22, NULL, '2', NULL, '1', 'enabled', NULL, '', '2020-08-08 02:09:25', '2020-08-08 02:09:25', NULL),
(24, 'BLN00000024', 'renjitha', 'gfhyfg', 'fhf', 'sherin.taurus+11@gmail.com', '46456', '3', NULL, '1', NULL, NULL, NULL, 21, '$2y$10$M46wdDMBG1nlO9gD5Mdjku.ukkWNW0yKVa2kzJVQZlgmx//iwgns6', '0', NULL, '1', 'enabled', NULL, '', '2020-08-08 04:27:53', '2020-08-08 04:27:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menulist`
--

CREATE TABLE `menulist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menulist`
--

INSERT INTO `menulist` (`id`, `name`) VALUES
(1, 'sidebar menu'),
(2, 'top menu');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `href`, `icon`, `slug`, `parent_id`, `menu_id`, `sequence`) VALUES
(1, 'Dashboard', '/admin/home', 'cil-speedometer', 'link', NULL, 1, 1),
(4, 'Admin Users', '/admin/users', 'cil-user', 'link', NULL, 1, 3),
(6, 'Edit menu elements', '/menu/element', NULL, 'link', 2, 1, 2),
(16, 'Breadcrumb', '/base/breadcrumb', NULL, 'link', 15, 1, 18),
(17, 'Cards', '/base/cards', NULL, 'link', 15, 1, 19),
(18, 'Carousel', '/base/carousel', NULL, 'link', 15, 1, 20),
(19, 'Collapse', '/base/collapse', NULL, 'link', 15, 1, 21),
(20, 'Forms', '/base/forms', NULL, 'link', 15, 1, 22),
(21, 'Jumbotron', '/base/jumbotron', NULL, 'link', 15, 1, 23),
(22, 'List group', '/base/list-group', NULL, 'link', 15, 1, 24),
(23, 'Navs', '/base/navs', NULL, 'link', 15, 1, 25),
(24, 'Pagination', '/base/pagination', NULL, 'link', 15, 1, 26),
(25, 'Popovers', '/base/popovers', NULL, 'link', 15, 1, 27),
(26, 'Progress', '/base/progress', NULL, 'link', 15, 1, 28),
(27, 'Scrollspy', '/base/scrollspy', NULL, 'link', 15, 1, 29),
(28, 'Switches', '/base/switches', NULL, 'link', 15, 1, 30),
(29, 'Tables', '/base/tables', NULL, 'link', 15, 1, 31),
(30, 'Tabs', '/base/tabs', NULL, 'link', 15, 1, 32),
(31, 'Tooltips', '/base/tooltips', NULL, 'link', 15, 1, 33),
(56, 'Dashboard', '/', NULL, 'link', 55, 2, 56),
(57, 'Notes', '/notes', NULL, 'link', 55, 2, 57),
(58, 'Users', '/users', NULL, 'link', 55, 2, 58),
(60, 'Edit menu', '/menu/menu', NULL, 'link', 59, 2, 60),
(61, 'Edit menu elements', '/menu/element', NULL, 'link', 59, 2, 61),
(62, 'Edit roles', '/roles', NULL, 'link', 59, 2, 62),
(63, 'Media', '/media', NULL, 'link', 59, 2, 63),
(64, 'BREAD', '/bread', NULL, 'link', 59, 2, 64),
(65, 'Red Genie', '/admin/red_genie', 'cil-user', 'link', NULL, 1, 17),
(66, 'Sales Genie', '/admin/sales_genie', 'cil-user', 'link', NULL, 1, 34),
(67, 'Settings', '/admin/settings', 'cil-settings', 'link', NULL, 1, 41),
(68, 'Resource Center Mgmnt', '/admin/resource_center', 'cil-library', 'link', NULL, 1, 40),
(70, 'Statements', '/admin/points', 'cil-dollar', 'link', NULL, 1, 36),
(71, 'Red Points', '/admin/points', NULL, 'link', 69, 1, 42),
(72, 'Networks', 'admin/network', 'cil-circle', 'link', NULL, 1, 39),
(73, 'Customers', 'admin/customers', 'cil-user', 'link', NULL, 1, 35),
(74, 'Redemption Requests', '/admin/redumptions', 'cil-envelope-letter', 'link', NULL, 1, 37),
(75, 'Root Node', '/admin/root_node', 'cil-user', 'link', NULL, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE `menu_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menus_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`id`, `role_name`, `menus_id`) VALUES
(22, 'user', 16),
(23, 'admin', 16),
(24, 'user', 17),
(25, 'admin', 17),
(26, 'user', 18),
(27, 'admin', 18),
(28, 'user', 19),
(29, 'admin', 19),
(30, 'user', 20),
(31, 'admin', 20),
(32, 'user', 21),
(33, 'admin', 21),
(34, 'user', 22),
(35, 'admin', 22),
(36, 'user', 23),
(37, 'admin', 23),
(38, 'user', 24),
(39, 'admin', 24),
(40, 'user', 25),
(41, 'admin', 25),
(42, 'user', 26),
(43, 'admin', 26),
(44, 'user', 27),
(45, 'admin', 27),
(46, 'user', 28),
(47, 'admin', 28),
(48, 'user', 29),
(49, 'admin', 29),
(50, 'user', 30),
(51, 'admin', 30),
(52, 'user', 31),
(53, 'admin', 31),
(105, 'guest', 56),
(106, 'user', 56),
(107, 'admin', 56),
(108, 'user', 57),
(109, 'admin', 57),
(110, 'admin', 58),
(112, 'admin', 60),
(113, 'admin', 61),
(114, 'admin', 62),
(115, 'admin', 63),
(116, 'admin', 64),
(122, 'admin', 6),
(148, 'admin', 4),
(149, 'admin', 65),
(150, 'user', 65),
(151, 'admin', 66),
(152, 'user', 66),
(153, 'admin', 67),
(154, 'user', 67),
(155, 'admin', 1),
(156, 'user', 1),
(163, 'admin', 68),
(164, 'user', 68),
(173, 'admin', 71),
(174, 'user', 71),
(201, 'admin', 72),
(202, 'user', 72),
(203, 'admin', 73),
(204, 'user', 73),
(209, 'admin', 70),
(210, 'user', 70),
(213, 'admin', 74),
(214, 'user', 74),
(217, 'admin', 75),
(218, 'user', 75);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_11_085455_create_notes_table', 1),
(5, '2019_10_12_115248_create_status_table', 1),
(6, '2019_11_08_102827_create_menus_table', 1),
(7, '2019_11_13_092213_create_menurole_table', 1),
(8, '2019_12_10_092113_create_permission_tables', 1),
(9, '2019_12_11_091036_create_menulist_table', 1),
(10, '2019_12_18_092518_create_role_hierarchy_table', 1),
(11, '2020_01_07_093259_create_folder_table', 1),
(12, '2020_01_08_184500_create_media_table', 1),
(13, '2020_01_21_161241_create_form_field_table', 1),
(14, '2020_01_21_161242_create_form_table', 1),
(15, '2020_01_21_161243_create_example_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 3),
(2, 'App\\User', 4),
(2, 'App\\User', 5),
(2, 'App\\User', 6),
(2, 'App\\User', 7),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(2, 'App\\User', 11),
(2, 'App\\User', 12),
(2, 'App\\User', 13),
(4, 'App\\User', 14),
(1, 'App\\User', 15),
(2, 'App\\User', 16),
(1, 'App\\User', 17),
(1, 'App\\User', 19),
(2, 'App\\User', 19),
(1, 'App\\User', 20);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sherin.taurus@gmail.com', '$2y$10$rtcDZQ9dW3q2eg9rq.evbOeybFNNp7IX9hYS/01STlPIkfw.TBXRu', '2020-03-19 00:49:33'),
('sherin.taurus@gmail.com', 'cBvVvQdeZH9G55OCMLWHNFL9DLvqBk5tycv5qtS4IUKj7Xcr9u1smP0GEsvu', '2020-08-07 01:19:58'),
('sherin.taurus@gmail.com', 'EvLZLj9tP3ICdwFzDDaTxn52aBxi8VmDmqh9IYohBxjz4inmFrioR9iW0QfM', '2020-08-07 01:22:05');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'browse bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(2, 'read bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(3, 'edit bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(4, 'add bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(5, 'delete bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `resource_center`
--

CREATE TABLE `resource_center` (
  `id` int(11) NOT NULL,
  `resource_for` enum('1','2','3') NOT NULL DEFAULT '3' COMMENT '1 : for redgenie, 2 : for salesgenie, 3 : for customer',
  `type` enum('1','2','3','4') NOT NULL DEFAULT '2' COMMENT '1:trainingmanual, 2:article, 3: casestudies, 4:videos',
  `title` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `is_featured` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0:no, 1:yes',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0:desabled, 1:enabled',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource_center`
--

INSERT INTO `resource_center` (`id`, `resource_for`, `type`, `title`, `link`, `is_featured`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', '1', '00', 'http://localhost/refer2gain/public/upload/resources/test1.pdf', '1', '1', '2020-03-27 00:13:08', '2020-05-02 03:34:49', NULL),
(2, '1', '2', 'test2', 'http://localhost/refer2gain/public/upload/resources/dummy.pdf', '1', '1', '2020-03-27 01:17:39', '2020-05-02 03:37:07', NULL),
(3, '1', '3', 'test3', 'http://localhost/refer2gain/public/upload/resources/test.pdf', '1', '1', NULL, '2020-05-02 03:34:34', NULL),
(4, '1', '4', 'test4', 'http://localhost/refer2gain/public/upload/resources/video1.mp4', '1', '1', NULL, '2020-05-02 03:33:54', NULL),
(5, '1', '4', 'test5', 'http://localhost/refer2gain/public/upload/resources/video2.mp4', '0', '1', NULL, '2020-05-02 04:01:47', NULL),
(6, '1', '2', 'qqq', 'qq', '0', '1', '2020-05-14 07:01:29', '2020-05-14 07:01:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-02-20 00:47:14', '2020-02-20 00:47:14'),
(2, 'user', 'web', '2020-02-20 00:47:14', '2020-02-20 00:47:14'),
(3, 'guest', 'web', '2020-02-20 00:47:14', '2020-02-20 00:47:14'),
(4, 'redgenie', 'web', '2020-03-12 00:00:34', '2020-03-12 00:00:34'),
(5, 'salesgenie', 'web', '2020-03-12 00:00:46', '2020-03-12 00:00:46'),
(6, 'customer', 'web', '2020-03-12 00:00:58', '2020-03-12 00:00:58');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `role_hierarchy`
--

CREATE TABLE `role_hierarchy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `hierarchy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_hierarchy`
--

INSERT INTO `role_hierarchy` (`id`, `role_id`, `hierarchy`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `settings` varchar(150) NOT NULL,
  `value` decimal(5,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `settings`, `value`, `created_at`, `updated_at`) VALUES
(1, 'pay out %\r\n', '10.00', NULL, '2020-08-08 01:28:34'),
(2, 'referal commision %', '60.00', NULL, NULL),
(3, 'parent commision %', '25.00', NULL, NULL),
(4, 'grand parent commision %', '15.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `class`) VALUES
(1, 'ongoing', 'badge badge-pill badge-primary'),
(2, 'stopped', 'badge badge-pill badge-secondary'),
(3, 'completed', 'badge badge-pill badge-success'),
(4, 'expired', 'badge badge-pill badge-warning');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuroles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `menuroles`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@refer2gain.com', '2020-02-20 00:47:14', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user,admin', 'KfyEsCLbjd03RfWLvGN5bGlPZtMI5Q0WGpr5v56PdoSmwUYsroUvHmdbfduo', '2020-02-20 00:47:14', '2020-03-19 03:17:33', NULL),
(2, 'tt', 'tt@gmail.com', '2020-04-29 18:30:00', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', NULL, '2020-04-29 18:30:00', '2020-04-30 00:44:41', NULL),
(3, 'admin', 'admin@gmail.com', NULL, '$2y$10$fG7gDLpt8JetASBcW0tZ6ezWViomJQmQUsjFPlVYaid6SMq2oAN3.', 'user', NULL, '2020-04-30 03:54:28', '2020-04-30 03:54:28', NULL),
(6, 'sherin', 'sherin.taurus@gmail.com', NULL, '$2y$10$svwNBjcnXaFo/h5ZuV/j5ecNeSo.RUwqJCFmsxLWSEfsq4QB6OnAi', 'user', NULL, '2020-04-30 04:06:42', '2020-04-30 04:06:42', NULL),
(7, 'qqq', 'q@g.com', NULL, '$2y$10$gNxI1t2tGh5vza6JVZ5zy.RLMTJo/XqIA.a8I24v/mV8w4sx62bna', 'user', NULL, '2020-08-06 00:15:09', '2020-08-06 00:15:09', NULL),
(8, 'sherin', 'sherin.taurus+1@gmail.com', NULL, '$2y$10$BJcE.PtcQGZlxnABWC0UhuFoEdHhkr5YOpoqUF7lQICz6uvEHOUWW', 'user', NULL, '2020-08-06 00:22:02', '2020-08-06 00:22:02', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bln_account_type`
--
ALTER TABLE `bln_account_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bln_redumption_request`
--
ALTER TABLE `bln_redumption_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bln_roles`
--
ALTER TABLE `bln_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bln_statements`
--
ALTER TABLE `bln_statements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folder`
--
ALTER TABLE `folder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_users`
--
ALTER TABLE `frontend_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menulist`
--
ALTER TABLE `menulist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource_center`
--
ALTER TABLE `resource_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bln_account_type`
--
ALTER TABLE `bln_account_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bln_redumption_request`
--
ALTER TABLE `bln_redumption_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bln_roles`
--
ALTER TABLE `bln_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bln_statements`
--
ALTER TABLE `bln_statements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `folder`
--
ALTER TABLE `folder`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `frontend_users`
--
ALTER TABLE `frontend_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `menulist`
--
ALTER TABLE `menulist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `menu_role`
--
ALTER TABLE `menu_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `resource_center`
--
ALTER TABLE `resource_center`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
