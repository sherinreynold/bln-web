<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
class adminCreateSuccessEmail extends Mailable
{
    use Queueable, SerializesModels;
/**
* Create a new message instance.
*
* @return void
*/
public $mailData;
public function __construct($mailData)
{
    $this->mailData = $mailData;
}
/**
* Build the message.
*
* @return $this
*/
public function build()
{
    $input = array(
        'name'     => $this->mailData['name'],
        'username'     => $this->mailData['username'],
        'password'     => $this->mailData['password'],
        );
    return $this->from('admin@programmingfields.com')->subject('Refer To Gain')->view('emails.admin_create_success_mail')->with(['inputs' => $input ]);
}
}
