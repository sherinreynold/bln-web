<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Statements;
use App\Frontend;
use PDF;

class PointController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';
		if($request->has('search')){
            $search = $request->input('search');
        }
		
        $points = Statements::leftjoin('frontend_users as fu', 'fu.id', '=', 'bln_statements.fk_frontend_user_id');
        
        if($request->has('start_date') && $request->has('end_date') && $request->input('start_date') !='' && $request->input('end_date') != ''){
            $points=$points->whereBetween('bln_statements.created_at', [$request->input('start_date'), $request->input('end_date')]);
        }
		elseif($request->has('start_date') && $request->input('start_date') != ''){
            $start_date = $request->input('start_date');
            $points=$points->whereDate('bln_statements.created_at', '>=', $start_date);
        }elseif($request->has('end_date') && $request->input('end_date') != ''){
            $end_date = $request->input('end_date');
            $points=$points->whereDate('bln_statements.created_at', '<=', $end_date );
        }

        if($search != ''){

            $points=$points->Where('fu.code', 'like', '%' . $search . '%');
                             // ->orWhere('red_genies.code', 'like', '%' . $search . '%')
                             //->orWhere('sg.code', 'like', '%' . $search . '%');

        }

        if($request->input('export')){
            
            return $this->downloadPDF($points);
        }

        $points=$points->select(['bln_statements.*','fu.code','fu.name'])
        ->orderBy('bln_statements.id', 'desc')
                       ->paginate(5);
        //echo '<pre>';print_r($points->ToArray());exit;
        $breadcrumbs[0]['title']='Statements';
		
        return view('dashboard.points.list',compact('breadcrumbs','points'));
    }



    public function create()
    {
       $users = Frontend::all(['name','id','code','account_status'])
       ->where('account_status','enabled')
       ->sortBy("name")->toArray();
       

        $breadcrumbs[0]['title']='Red Points';
        $breadcrumbs[0]['link']=url('admin/points');
        $breadcrumbs[1]['title']='Add';
	   return view('dashboard.points.create', [ 'users' => $users, 'breadcrumbs' => $breadcrumbs ]);
   }


   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'point'           => 'required',
			
        ]);
            

        $red_points=new Statements();

        
            $red_points->fk_frontend_user_id = $request->input('user_id'); 
        
	   
        $red_points->point = $request->input('point');
        $red_points->refferal_code = 'admin';
        $red_points->description = $request->input('description');
        $red_points->save();
        
        $invoice_no='adm'.$red_points->id;
        $p = Statements::find($red_points->id);
        $p->invoice_no=$invoice_no;
        $p->save();
		
		
		

        $request->session()->flash('message', 'Points addedd Successfully');
        return redirect()->route('points.index');
    }

    
    public function view($id)
    {
        $credits = Statements::where('bln_statements.invoice_no',$id)
        ->join('frontend_users as fu','fu.id','=','bln_statements.fk_frontend_user_id')
        ->select(['bln_statements.*','fu.code','fu.name'])->get();
        //echo '<pre>';print_r($credits->toArray());exit;
		
        $breadcrumbs[0]['title']='Red Points';
        $breadcrumbs[0]['link']=url('admin/points');
        $breadcrumbs[1]['title']='Details';
		
        return view('dashboard.points.show',compact('breadcrumbs','credits'));
    }



    public function edit($id)
    {
        $users = Frontend::all(['name','id','code','account_status'])
       ->where('account_status','enabled')
       ->sortBy("name")->toArray();


        $note = Statements::where('id',$id)
                ->get(['*']);	
        //echo '<pre>';print_r($note);exit;
        $breadcrumbs[0]['title']='Red Points';
        $breadcrumbs[0]['link']=url('admin/points');
        $breadcrumbs[1]['title']='Edit';
        return view('dashboard.points.edit', ['note' => $note[0] ,'breadcrumbs' => $breadcrumbs, 'users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            'point'           => 'required',
			
        ]);
		
		
        
        
        $red_points=Statements::find($id);

        
            $red_points->fk_frontend_user_id = $request->input('user_id'); 
    
	   
        $red_points->point = $request->input('point');
        //$red_points->refferal_code = 'admin';
        $red_points->save();
		
		
		

        $request->session()->flash('message', 'Points updated Successfully');
        return redirect()->route('points.index');
    }


    public function downloadPDF($points) {
        
        $points=$points->select(['bln_statements.*','fu.code','fu.name'])
        ->orderBy('bln_statements.id', 'desc')->get();

        //echo '<pre>';print_r($points);exit;

        $pdf = PDF::loadView('pdf.statement_pdf', compact('points'));
        
        return $pdf->download('disney.pdf');
}

   
	
	
	
	
}
