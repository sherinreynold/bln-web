<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\RedumptionRequest;
use App\Models\Statements;
use App\Frontend;

class RedumptionRequestController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';
		if($request->has('search')){
            $search = $request->input('search');
        }
		
        $redumption_requests = RedumptionRequest::leftjoin('frontend_users as fu', 'fu.id', '=', 'bln_redumption_request.fk_frontend_user_id');
        
        if($request->has('start_date') && $request->has('end_date') && $request->input('start_date') !='' && $request->input('end_date') != ''){
            $redumption_requests=$redumption_requests->whereBetween('bln_redumption_request.created_at', [$request->input('start_date'), $request->input('end_date')]);
        }
		elseif($request->has('start_date') && $request->input('start_date') != ''){
            $start_date = $request->input('start_date');
            $redumption_requests=$redumption_requests->whereDate('bln_redumption_request.created_at', '>=', $start_date);
        }elseif($request->has('end_date') && $request->input('end_date') != ''){
            $end_date = $request->input('end_date');
            $redumption_requests=$redumption_requests->whereDate('bln_redumption_request.created_at', '<=', $end_date );
        }

        if($search != ''){

            $redumption_requests=$redumption_requests->Where('fu.code', 'like', '%' . $search . '%');
                             // ->orWhere('red_genies.code', 'like', '%' . $search . '%')
                             //->orWhere('sg.code', 'like', '%' . $search . '%');

        }

        $redumption_requests=$redumption_requests->select(['bln_redumption_request.*','fu.code','fu.name'])
        ->orderBy('bln_redumption_request.id', 'desc')
                       ->paginate(10);
       //echo '<pre>';print_r($redumption_requests->ToArray());exit;
        $breadcrumbs[0]['title']='Redumtion Requests';
		
        return view('dashboard.points.redumption_request',compact('breadcrumbs','redumption_requests'));
    }

    
    public function approve_rew(Request $request)
    {
       // echo 'sd';exit;
       $id=$request->input('id');
       $req=RedumptionRequest::find($id);


       $red_points=new Statements();

       //insert point 
       $red_points->fk_frontend_user_id = $req->fk_frontend_user_id; 
       $red_points->point = $req->amount;
       $red_points->type = '3';
       $red_points->refferal_code = $id;
       $red_points->description = 'redumption request';
       $red_points->invoice_no='REDUM_'.$id;
       $red_points->save();
   

       //update req
       $req->status = 'approved';
       $req->save();

       echo 'success';exit;

    }
	
	
	
}
