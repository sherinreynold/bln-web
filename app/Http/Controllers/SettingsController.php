<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Settings;


class SettingsController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
        $settings = DB::table('settings')->paginate(10);

        $breadcrumbs[0]['title']='Settings';
		
        return view('dashboard.settings.list', ['settings' => $settings,'breadcrumbs' => $breadcrumbs]);
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = Settings::find($id);

        $breadcrumbs[0]['title']='Settings';
        $breadcrumbs[0]['link']=url('admin/settings');
        $breadcrumbs[1]['title']='Edit';
        
        return view('dashboard.settings.edit', ['settings' => $settings,'breadcrumbs'=>$breadcrumbs ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            
			'value'           => 'required'
        ]);
		
		
        $note = Settings::find($id);
        
		
		 
       
        $note->value     = $request->input('value');
       
		
		
		
        $note->save();
        $request->session()->flash('message', 'Successfully edited Settings');
        return redirect()->route('settings.index');
    }

   
			
	}
