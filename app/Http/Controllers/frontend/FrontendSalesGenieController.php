<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Mail\adminCreateSuccessEmail;
use App\Mail\InvitationEmail;
use App\Models\SalesGenies;
use App\Models\RedGenies;
use App\Models\Customers;
use App\Frontend;
use App\Http\Controllers\Controller;

class FrontendSalesGenieController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    
    public function create()
    {
        $login_info = auth()->guard('frontend')->user()->ToArray();
       
        if($login_info['user_type']=='redgenie'){
            $user = RedGenies::where('fk_frontend_user_id',$login_info['id'])->first()->ToArray(); 
        }
        
        $sg_id = array('fk_red_genie_id' =>$user['id'], 'type' =>'2');
        $salesgenie_plus = SalesGenies::where( $sg_id )->get(['first_name','last_name','id','code'])->sortBy("first_name")->ToArray(); 
       

        return view('frontend.customers.createSalesGenie')->with(['login_info' =>$login_info, 'salesgenie_plus' =>$salesgenie_plus]);	 
        
   }

    
    public function store(Request $request)
    {
        $login_info = auth()->guard('frontend')->user()->ToArray();

        $fk_redgenie_id=$fk_salesgenie_id=0;
 
        if($login_info['user_type']=='redgenie'){
            $rg = RedGenies::where('fk_frontend_user_id',$login_info['id'])->first()->ToArray();
            $fk_redgenie_id=$rg['id'];
            }

        if($login_info['user_type']=='salesgenie'){
        $sg = SalesGenies::where('fk_frontend_user_id',$login_info['id'])->first()->ToArray();
        $fk_salesgenie_id=$sg['id'];

        if($sg['type']=='1'){
            return redirect()->route('create_salesgenie')->withErrors('You have no access to create new sales genie');
        }
       
        }


        $validatedData = $request->validate([
            
            'name'           => 'required',
			//'l_name'           => 'required',
			'username'           => 'required',
			'password'      => 'required',
			'type'             => 'required'
        ]);
       // $user = auth()->user();


        /* parent limit check  */

        if($login_info['user_type']=='salesgenie'){
            $pid = $fk_salesgenie_id;
            $p_limit=SalesGenies::where('fk_sales_genie_id',$pid)
                                  ->where('type',$request->input('type'))->count();
            }else{

                if($request->input('type')=="2"){
            $pid = $fk_redgenie_id; 
            $p_limit=SalesGenies::where('fk_red_genie_id',$pid)
                                 ->where('type',$request->input('type'))->count();  
            }else{

                if($request->input('salesgenieplus_id')==''){
                    return redirect()->route('create_salesgenie')->withErrors('Please select a salesgenie plus');
                }
                
                $pid = $request->input('salesgenieplus_id');
                $p_limit=SalesGenies::where('fk_sales_genie_id',$pid)
                                  ->where('type',$request->input('type'))->count();
            }
        }
            
            if ($p_limit >=5) {
                return redirect()->route('create_salesgenie')->withErrors('You have exceed the limit');
                   }
           
           /* parent limit check ends */
	   
	   
	   if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('create_salesgenie')->withErrors('email id already exists');
             }
			 
	    if (Frontend::where('username', '=', $request->input('username'))->exists()) {
          return redirect()->route('create_salesgenie')->withErrors('username already exists');
        }		 
	   
	   
	    $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->username = $request->input('username');
		$frontend_user->user_type = 'salesgenie';
		$frontend_user->password = Hash::make($request->input('password'));
		$frontend_user->save();
		
		
		$code=str_pad($frontend_user->id,10, '0', STR_PAD_LEFT);
	    $user_code="SG".$code;


        if($login_info['user_type']=='redgenie' && $request->input('type')=="1"){
            $fk_salesgenie_id=$request->input('salesgenieplus_id');
            $fk_redgenie_id=0;
        }
	   
        $note = new SalesGenies();
		$note->code = $user_code;
		$note->fk_frontend_user_id = $frontend_user->id;
        $note->first_name     = $request->input('name');
		$note->mobile = $request->input('mobile');
        $note->fk_red_genie_id = $fk_redgenie_id;
        $note->fk_sales_genie_id = $fk_salesgenie_id;
		$note->type = $request->input('type');
		
        
        $note->save();

        $mailData = array(
            'name'     => $request->input('name'),
            'username'     => $request->input('username'),
            'password'     => $request->input('password'),
        );
        Mail::to($request->input('email_id'))->send(new adminCreateSuccessEmail($mailData));

        $request->session()->flash('message', 'Successfully created Sales Genie');
        return redirect()->route('create_salesgenie');
    }

  
    public function destroy($id)
    {
        $sg = SalesGenies::find($id);
		
        if($sg){
            DB::table('frontend_users')->delete($sg->fk_frontend_user_id);
            $sg->delete();
        }
		
        return redirect()->route('sales_genie.index');
    }


    public function invites()
    {
        $login_info = auth()->guard('frontend')->user()->ToArray();
        return view('frontend.customers.createCustomer')->with(['login_info' =>$login_info]);	 
        
   }


    public function addInvites(Request $request){
       
        $login_info = auth()->guard('frontend')->user()->ToArray();

        if($login_info['user_type']=='salesgenie'){

        $validatedData = $request->validate([
            
            'name'           => 'required',
			
			'email_id'           => 'required'
        ]);
       
	   	    
			 
		 if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('invites',[$salesgenie_id])->withErrors('email id already exists');
             }
			 

        $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->user_type = 'customer';
		$frontend_user->save();			 
	   
	     
		$code=str_pad($frontend_user->id,10, '0', STR_PAD_LEFT);
	    $user_code="C".$code; 
		 
        $c = new Customers();
		$c->code = $user_code;
		$c->fk_frontend_user_id = $frontend_user->id;
        $c->name     = $request->input('name');
		$c->mobile = $request->input('mobile');
		$c->type = $request->input('type');
		
        //$c->ref_by = 2;
        $ref_info=SalesGenies::where('fk_frontend_user_id', '=', $login_info['id'])->get()->ToArray();

		$c->ref_code = $ref_info[0]['code'];
       
        
        $c->save();
        
        $mailData = array(
            'name'     => $request->input('name'),
            'action'     => url('registration/'.base64_encode($user_code)),
        );
        Mail::to($request->input('email_id'))->send(new InvitationEmail($mailData));

        $request->session()->flash('message', 'Registration link sent to '.$request->input('email_id'));
        
        return redirect()->route('invites');
        
            }

    }
	
	
	
}
