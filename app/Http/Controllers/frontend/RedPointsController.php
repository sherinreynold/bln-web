<?php
namespace App\Http\Controllers\frontend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Statements;
use App\Models\Settings;
use App\Frontend;
use App\Http\Controllers\Controller;
class RedPointsController extends Controller
{
 
    public function add(Request $request){
        
        
        if(isset($_SERVER['HTTP_R2G_SECRET_ACCESS_KEY']) && $_SERVER['HTTP_R2G_SECRET_ACCESS_KEY']==123){
            
            if($request->post('referral_code') && $request->post('customer_id') && $request->post('invoice_amount')
            && $request->post('invoice_number') && $request->post('currency') && $request->post('invoice_referrence_id')){
             
                $settings=Settings::all()->ToArray();

                $pay_out_percentage=$settings[0]['value'];
                $pay_out_value=$request->post('invoice_amount')*$pay_out_percentage/100;

                

                         
             
              //amt % calculation sections
              $ref_percentage=$settings[1]['value'];
              $ref_parent_percentage=$settings[2]['value'];
              $ref_grand_parent_percentage=$settings[3]['value'];

              //echo $ref_grand_parent_percentage;exit;

              
             

            
            $c_info = Frontend::where('code', '=', $request->post('referral_code'))->get()->ToArray();
            //echo '<pre>';print_r($c_info);exit;
        
            if(!empty($c_info)){

        
           $refferer_id=$c_info[0]['parent_id'];
           $refferer_point=$pay_out_value*$ref_percentage/100;  
            
            //add points refferer
            $this->addPointToParent($refferer_id,$refferer_point,$request);

           if($refferer_id != 0){
             
            $reffere_info=Frontend::where('id', '=', $refferer_id)->get()->ToArray();

            if(!empty($reffere_info)){
              
                $r_parent=$reffere_info[0]['parent_id'];
                $r_parent_point=$pay_out_value*$ref_parent_percentage/100;

                 //add points parent
                 $this->addPointToParent($r_parent,$r_parent_point,$request);


                 if($r_parent != 0){
             
                    $r_parent_info=Frontend::where('id', '=', $r_parent)->get()->ToArray();
        
                    if(!empty($r_parent_info)){
                      
                        $r_grand_parent=$r_parent_info[0]['parent_id'];
                        $r_grand_parent_point=$pay_out_value*$ref_grand_parent_percentage/100;
        
                         //add points parent
                         $this->addPointToParent($r_grand_parent,$r_grand_parent_point,$request);
        
                    }
        
                   }



            }

           }



            $resp['status']='true';
            $resp['info']='Refferal point addedd successfully';
            $resp['result']=array('customer_id'=>$request->post('customer_id'));

            echo json_encode($resp);exit;

            }

        }else{echo 'parameters missing';}
            }
            else{
                echo 'no access';
            }


        
    }

    public function addPointToParent($parent_id,$r_parent_point,$request){

        
        $ref_code=$request->post('referral_code');   
        $invoice_amount=$request->post('invoice_amount');
        $invoice_number=$request->post('invoice_number');
        $currency=$request->post('currency');
        $invoice_referrence_id=$request->post('invoice_referrence_id');
        //echo $r_parent_point;exit;

                $points= new Statements();
                $points->refferal_code = $ref_code;
                $points->rl_customer_id =$request->post('customer_id');
                $points->invoice_amount  =$invoice_amount;
                $points->currency  =$currency;
                $points->invoice_no  =$invoice_number;
                $points->invoice_reff_id  =$invoice_referrence_id;
                $points->point  =$r_parent_point;
                
                $points->fk_frontend_user_id=$parent_id;
                $points->save();
                return true;
    }
   
}