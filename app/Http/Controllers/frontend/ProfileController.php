<?php

namespace App\Http\Controllers\frontend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\RedGenies;
use App\Models\SalesGenies;
use App\Models\Customers;
use App\Frontend;
use App\Models\Experiences;
use App\Http\Controllers\Controller;
class ProfileController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
        $login_info = auth()->guard('frontend')->user()->ToArray();
		
		if($login_info['user_type']=='redgenie'){
		
		$user = RedGenies::where('fk_frontend_user_id',$login_info['id'])->first()->ToArray(); 
		$exp  = Experiences::where('fk_frontend_user_id',$login_info['id'])->get();
		//echo '<pre>';print_r($exp);exit;
		
		return view('frontend.customers.redgenie_profile')->with(['login_info' =>$login_info,'user' => $user,'exp' => $exp]);
        }
        if($login_info['user_type']=='salesgenie'){
        
        $user = SalesGenies::where('fk_frontend_user_id',$login_info['id'])->first()->ToArray();
        $exp  = Experiences::where('fk_frontend_user_id',$login_info['id'])->get(); 
        return view('frontend.customers.salesgenie_profile')->with(['login_info' =>$login_info,'user' => $user,'exp' => $exp]);
        
        }
        else{
			
        $user = Customers::where('fk_frontend_user_id',$login_info['id'])->first()->ToArray();	
		return view('frontend.customers.customer_profile')->with(['login_info' =>$login_info,'user' => $user]);	
		}
		
    }
public function update(Request $request)
    {
		//echo $request->input('name');exit;
         $validatedData = $request->validate([
            
            'name'           => 'required',
			'email'           => 'required',
			'phone'           => 'required',
			'address'           => 'required'
        ]);
		//echo "gdfgdF";exit;
		 $login_info = auth()->guard('frontend')->user()->ToArray();
         
         if($login_info['user_type']=='redgenie'){

        $note = RedGenies::where('fk_frontend_user_id',$login_info['id'])->first();
	 
        $note->first_name     = $request->input('name');
		$note->mobile = $request->input('phone');
        $note->address = $request->input('address');
		
        $note->save();
        
         }else if($login_info['user_type']=='salesgenie'){

        $note = SalesGenies::where('fk_frontend_user_id',$login_info['id'])->first();
	 
        $note->first_name     = $request->input('name');
		$note->mobile = $request->input('phone');
        $note->address = $request->input('address');
		
        $note->save();

         }else{

        $note = Customers::where('fk_frontend_user_id',$login_info['id'])->first();
	 
        $note->name     = $request->input('name');
		$note->mobile = $request->input('phone');
        $note->address = $request->input('address');

        $note->save();

         }
		
		$note1 = Frontend::find($note->fk_frontend_user_id);
		
		$note1->email = $request->input('email');
		$note1->save();
		
		
        $request->session()->flash('message', 'Profile edited Successfully');
		
       return redirect()->route('profile');
    }
	
public function expUpdate(Request $request){

    $login_info = auth()->guard('frontend')->user()->ToArray();
	$exp = new Experiences();
	$exp->fk_frontend_user_id=$login_info['id'];
	$exp->title = $request->input('title');
	$exp->description = $request->input('description');
	$exp->save();
	
	$request->session()->flash('message', 'Profile edited Successfully');
		
    return redirect()->route('profile');


}

public function expRemove($id){


    DB::table('experience')->where('id', '=', $id)->delete();
    return redirect()->route('profile');


}

public function resourceCenter(){

    $login_info = auth()->guard('frontend')->user()->ToArray();

    if($login_info['user_type']=='redgenie'){
        $r_for='1';
    }else if($login_info['user_type']=='salesgenie'){
        $r_for='2';
    }else{
        $r_for='3';
    }

    $r_status='1'; //enabled resources only
    
    $rc_featured = DB::table('resource_center')->where('is_featured','1')->where('resource_for',$r_for)
    ->where('status',$r_status)->get();

    $rc_training_manual = DB::table('resource_center')->where('type','1')->where('resource_for',$r_for)
    ->where('status',$r_status)->get();
    $rc_article = DB::table('resource_center')->where('type','2')->where('resource_for',$r_for)
    ->where('status',$r_status)->get();
    $rc_case_studies = DB::table('resource_center')->where('type','3')->where('resource_for',$r_for)
    ->where('status',$r_status)->get();
    $rc_videos = DB::table('resource_center')->where('type','4')->where('resource_for',$r_for)
    ->where('status',$r_status)->get();

    //echo '<pre>';print_r($rc_training_manual);exit;
    $param_array['login_info']=$login_info;
    $param_array['rc_featured']=$rc_featured;
    $param_array['rc_training_manual']=$rc_training_manual;
    $param_array['rc_article']=$rc_article;
    $param_array['rc_case_studies']=$rc_case_studies;
    $param_array['rc_videos']=$rc_videos;

    return view('frontend.customers.resource_center')->with($param_array);

}

public function statements(){

    $login_info = auth()->guard('frontend')->user()->ToArray();
    return view('frontend.customers.statements')->with('login_info',$login_info);

}


//network
public function network($value='')
{
  $login_info = auth()->guard('frontend')->user()->ToArray();
  $userid=$login_info['id'];
  $usertype=$login_info['user_type'];
  $red_userid = array('fk_frontend_user_id' => $userid, );
  $res=[];

  if( $usertype=='redgenie'){   
	$res['red_gn']   = RedGenies::filter($red_userid)->first();
	$sg_id = array('fk_red_genie_id' =>$res['red_gn']['id']);
	$res['sales_gn'] = SalesGenies::where( $sg_id )->get()->ToArray(); 
	$sales_2=SalesGenies::filter($sg_id, '1')->get()->count(); 
	$sales_plus_2=SalesGenies::filter($sg_id, '2')->get()->count(); 
	$view='network';
  }else if( $usertype=='salesgenie'){
    $res['sales_gn'] = SalesGenies::filter($red_userid)->get()->ToArray(); 
    $sales_plus_id=$res['sales_gn'][0]['fk_red_genie_id'];
    $sales_plus_userid=$res['sales_gn'][0]['fk_sales_genie_id'];
    $view='network_salesgenie_plus';
	$sales_2=SalesGenies::filter($red_userid, '1')->get()->count(); 
	$sales_plus_2=SalesGenies::filter($red_userid, '2')->get()->count();

    if ($sales_plus_id=='0') {
		$redg_id = array('id' => $sales_plus_userid);  
		$res['sales_gn_plus'] = SalesGenies::filter($redg_id)->get()->ToArray();
		$sales_plus_id=$res['sales_gn_plus'][0]['fk_red_genie_id'];
		$view='network_salesgenie';
		$sales_2=0; 
		$sales_plus_2=0;
    }

    $res['sales_gn'] = SalesGenies::filter($red_userid)->get()->ToArray(); 
    $redg_id = array('id' =>   $sales_plus_id);  
    $res['red_gn'] = RedGenies::filter($redg_id)->first();
  }
  foreach ( $res['sales_gn'] as $key => $value) {
    $cust_id = array('ref_code' =>$value['code']);
    $cus = Customers::where( $cust_id)->get()->ToArray();
    $cus_plus =$cus_plus+ Customers::where( $cust_id)->get()->count(); 
    $res['sales_gn'][$key]['customers']=$cus;
    if ($value['type']=='2') {
      $sg_id = array('fk_sales_genie_id' =>$value['id']);
      $sales_gn_sub = SalesGenies::filter($sg_id, '1')->get()->ToArray();    
      $res['sales_gn'][$key]['sales_gn_sub']=$sales_gn_sub;
      foreach ($res['sales_gn'][$key]['sales_gn_sub'] as $key2 => $value2) {
        $cust_id_sub = array('ref_code' =>$value2['code']);
        $sub_cnt =Customers::filter($cust_id_sub)->get()->count();           
        $sub_cnt_tot =$sub_cnt_tot+ $sub_cnt;           
        $res['sales_gn'][$key]['sales_gn_sub'][$key2]['sales_gn_3_count']=$sub_cnt;
      }
    }
    $sales_gn_sub_plus = SalesGenies::filter($sg_id, '1')->get()->count(); 
    $res['sales_gn'][$key]['sales_gn_sub']['total_salesgen']=$sales_gn_sub_plus;
    $cus_tot =Customers::where( $cust_id)->get()->count(); 
    $res['sales_gn'][$key]['sales_gn_sub']['total_customer']=$cus_tot;
    $sales_3 =$sales_3 + SalesGenies::filter($sg_id, '1')->get()->count();
  }

  $res['customers_tot']=$cus_plus+$sub_cnt_tot;
  $res['sales_gen_tot']=$sales_2+$sales_3;
  $res['sales_gen_p_tot']=$sales_plus_2;
  $param_array['login_info']=$login_info;
  $param_array['res']=$res;

  return view('frontend.customers.'.$view)->with($param_array);
}


public function customers_row(Request $request)
{
  $value= $request->input('id');
  $res['sales_gn'] = SalesGenies::where('id',$value)->get()->ToArray(); 
  //Getting count
  $count = Customers::where('ref_code',$res['sales_gn'][0]['code'])->get()->count();
  $skip = 10;
  $limit = $count - $skip; // the limit
  $collection = Customers::where('ref_code',$res['sales_gn'][0]['code'])->skip($skip)->take($limit)->get();
  return view('frontend.customers.network_ajax')->with('cus', $collection);
}

public function getProfilePic($id,$profile='false'){

  if($profile == 'profile'){
  
    $default_url=asset('frontend/img/profile_pic_default.png');

  }else{
    $default_url=asset('frontend/img/avathar'.rand(2,4).'.png');
  }

  if (file_exists(public_path('upload/profile_pics/profile_pic_'.$id.'.png'))){
   $img_url=asset('upload/profile_pics/profile_pic_'.$id.'.png');
  }else{
   $img_url=$default_url;
  }

  $res['success'] = true;
  $res['image'] = $img_url;
  echo json_encode($res);exit;

}
    
}
