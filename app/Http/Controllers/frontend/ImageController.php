<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Customers;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
        //$you = auth()->guard('frontend')->user()->ToArray();
		
		return view('frontend.login.test')->with('data',$you);
    }


    public function uploadImage(Request $request)

    {
        $login_info = auth()->guard('frontend')->user()->ToArray();

        $image = $request->image;


        list($type, $image) = explode(';', $image);

        list(, $image)      = explode(',', $image);

        $image = base64_decode($image);

        $image_name= 'profile_pic_'.$login_info['id'].'.png';

        $path = public_path('upload/profile_pics/'.$image_name);


        file_put_contents($path, $image);

        return response()->json(['status'=>true]);

    }

    
}
