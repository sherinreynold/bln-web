<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegSuccessEmail;
use App\Mail\RegSuccessEmailToParent;
use App\Models\Customers;
use App\Http\Controllers\Controller;
use App\Frontend;
use App\Models\RedGenies;
use App\Models\SalesGenies;

class CustomersController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	
	public function register(request $request){
		
		$validatedData = $request->validate([
            
            'type'           => 'required',
			'username'         => 'required',
			'password'         => 'required', 
			'email_id'         => 'required',
			'phone'           => 'required'
        ]);
		
		if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
			return view('frontend.login.signup',$request->input())->withErrors('email id already exists');
             }
			 
	   if (Frontend::where('username', '=', $request->input('username'))->exists()) {
		return view('frontend.login.signup',$request->input())->withErrors('username already exists');
             }
			 
		$type=$request->input('type');
		
		
		//refferal sec start
		if($type=='customer'){
		    if($request->input('ref') != ''){
				
				if (SalesGenies::where('code', '=', $request->input('ref'))->exists()) {
					//$ref_info=SalesGenies::where('code', '=', $request->input('ref'))->get()->ToArray();
					
					//$ref_by=2;
					//$ref_id=$ref_info[0]['id'];

					$ref_code=$request->input('ref');
					
				}else{
					return view('frontend.login.signup',$request->input())->withErrors('Refferal id is invalid');
				}
			}else{
				
				return view('frontend.login.signup',$request->input())->withErrors('Refferal id is Required');
				
			}
			
		}
		
		
		if($type=='salesgenie'){
		    if($request->input('ref') != ''){
				
				if (RedGenies::where('code', '=', $request->input('ref'))->exists()) {
					$ref_info=RedGenies::where('code', '=', $request->input('ref'))->get()->ToArray();
					
					$redgenie_id=$ref_info[0]['id'];

					
					/* parent limit check  */

        
            $pid = $redgenie_id;
            $p_limit=SalesGenies::where('fk_red_genie_id',$pid)
                                  ->where('type',2)->count();
            
            if ($p_limit >=5) {
				return view('frontend.login.signup',$request->input())->withErrors('Parent have exceed the limit');
                
                   }
           
           /* parent limit check ends */

					
					

					
				}elseif(SalesGenies::where('code', '=', $request->input('ref'))->where('type',2)->exists()){

					$ref_info=SalesGenies::where('code', '=', $request->input('ref'))->get()->ToArray();
					
					$salesgenie_id=$ref_info[0]['id'];

					
					/* parent limit check  */

        
            $pid = $salesgenie_id;
            $p_limit=SalesGenies::where('fk_sales_genie_id',$pid)
                                  ->where('type',1)->count();
            
            if ($p_limit >=5) {
				return view('frontend.login.signup',$request->input())->withErrors('Parent have exceed the limit');
                
                   }
           
           /* parent limit check ends */

					
					



				}else{
					return view('frontend.login.signup',$request->input())->withErrors('Refferal SalesGenie+ id is invalid');
				}
			}else{
				return view('frontend.login.signup',$request->input())->withErrors('Refferal RedGenies id is Required');
				//return redirect()->route('frontend.signup')->withErrors('Refferal RedGenies id is Required');
				
			}
			
		}
			
			
		//refferal sec ends	
		
        
        $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->username = $request->input('username');
		$frontend_user->user_type = $type;
		$frontend_user->password = Hash::make($request->input('password'));
		$frontend_user->save();

	   
	    $code=str_pad($frontend_user->id,10, '0', STR_PAD_LEFT);
		
		if($type=='redgenie'){
	    
		   $user_code="RG".$code;

           $rg = new RedGenies();
		   $rg->code = $user_code;
		   $rg->fk_frontend_user_id = $frontend_user->id;
		   $rg->mobile = $request->input('phone');
          
        
           $rg->save();


        }

        if($type=='salesgenie'){
	    
		    $user_code="SG".$code;

            $sg = new SalesGenies();
		    $sg->code = $user_code;
		    $sg->fk_frontend_user_id = $frontend_user->id;
			$sg->mobile = $request->input('phone');
			if(isset($redgenie_id)){
			$sg->fk_red_genie_id=$redgenie_id;
			$sg->type='2';
			}
			elseif(isset($salesgenie_id)){
			$sg->fk_sales_genie_id=$salesgenie_id;
			
			}
    
            $sg->save();

        }
       if($type=='customer'){
	    
		    $user_code="C".$code;
								
						

        $c = new Customers();
		$c->code = $user_code;
		$c->fk_frontend_user_id = $frontend_user->id;
		$c->mobile = $request->input('phone');
		
		//$c->ref_by = $ref_by;
		$c->ref_code=$ref_code;
		//$c->ref_id = $ref_id;
		$c->type = 3;
       
        
        $c->save();
    
        //RL add customer api
		$add_api_params['company_name']='refer to gain';
		$add_api_params['username']=$request->input('username');
		$add_api_params['email']=$request->input('email_id');
		$add_api_params['password']=$request->input('password');
		$add_api_params['phone']=$request->input('phone');
		$add_api_params['country_code']="+91";
		$add_api_params['location']="Cochin";
		$add_api_params['is_terms_agreed']=true;
		$add_api_params['platform_user_type']=3;
		$add_api_params['referral_code']=$user_code;
		$add_api_params['parent_referral_code']=$ref_code;
			 $rl_add_customer=$this->rlApiAddCustomer($add_api_params);

			 //RL add customer api ends

        }		
		
		
        Mail::to($request->input('email_id'))->send(new RegSuccessEmail(array()));
		
		
		return redirect()->route('frontend.login')->with('message','Registration success.Please Login with user name and paassword');
		
	}
	
	
	
	//complete reg via link
	
	/*public function complete_registration($ref_id)
    {
		$ref_id=base64_decode($ref_id);
		
        //$reg_info = Customers::all()->find($ref_id);
		
		$reg_info = DB::table('customer as c')->join('frontend_users as fu', 'c.fk_frontend_user_id', '=', 'fu.id')->select('c.*','fu.email','fu.username')->where('c.code',$ref_id)->get()->ToArray();
		
		if(empty($reg_info)){
		return redirect()->route('frontend.signup')->withErrors('Refferal Link is invalid');
		}else{
			if($reg_info[0]->username != ''){
			return redirect()->route('frontend.login')->withErrors('Your registration already completed Please Login to continue.');
			}
		}
		//echo '<pre>';print_r($reg_info);exit;
        return view('frontend.customers.completeReg', ['reg_info' => $reg_info]);
    }*/
	
	public function completeRegistration($ref_id)
    {
		$ref_id=base64_decode($ref_id);
		
        //$reg_info = Customers::all()->find($ref_id);
		
		$reg_info = DB::table('customer as c')->join('frontend_users as fu', 'c.fk_frontend_user_id', '=', 'fu.id')->select('c.*','fu.email','fu.username')->where('c.code',$ref_id)->get()->ToArray();
		
		if(empty($reg_info)){
		return redirect()->route('frontend.signup')->withErrors('Refferal Link is invalid');
		}else{
			if($reg_info[0]->username != ''){
			return redirect()->route('frontend.login')->withErrors('Your registration already completed Please Login to continue.');
			}
		}
		//echo '<pre>';print_r($reg_info);exit;
        return view('frontend.customers.completeReg', ['reg_info' => $reg_info]);
    }
	
	public function updateRegistration($ref_id,Request $request){
		
		$validatedData = $request->validate([
            
            'type'           => 'required',
			'username'         => 'required',
			'password'         => 'required', 
			'phone'           => 'required'
        ]);
		
		if (Frontend::where('username', '=', $request->input('username'))->exists()) {
          return redirect()->route('complete_registration',[$ref_id])->withErrors('username already exists');
             }
			 
			 $f_user=Frontend::find($request->post('f_id'));
			 
			 $f_user->username = $request->post('username');
			 $f_user->password = Hash::make($request->input('password'));
			 $f_user->save();
			 
			 $c=Customers::find($request->post('c_id'));
			 
			 $c->mobile = $request->post('phone');
			 $c->save();
			 
			 //RL add customer api
			 $add_api_params['company_name']='refer to gain';
			 $add_api_params['username']=$request->input('username');
			 $add_api_params['email']=$f_user['email'];
			 $add_api_params['password']=$request->input('password');
			 $add_api_params['phone']=$request->input('phone');
			 $add_api_params['country_code']="+91";
			 $add_api_params['location']="Cochin";
			 $add_api_params['is_terms_agreed']=true;
			 $add_api_params['platform_user_type']=3;
			 $add_api_params['referral_code']=$c['code'];
			 $add_api_params['parent_referral_code']=$c['ref_code'];
			 $rl_add_customer=$this->rlApiAddCustomer($add_api_params);

			 //RL add customer api ends
             
			 Mail::to($f_user['email'])->send(new RegSuccessEmail(array()));

			 //success mail to parent
			 $parnet_info = DB::table('sales_genies as g')->join('frontend_users as fu', 'g.fk_frontend_user_id', '=', 'fu.id')
			 ->where('g.code',$c['ref_code'])->select('fu.*')->get()->ToArray();
			 
			 $mailData = array(
				'message'     => 'The user with name '.$c['name'].' is completed registration via your invitation link.',
				
			);
			 Mail::to($parnet_info[0]->email)->send(new RegSuccessEmailToParent($mailData));

			 
            //exit;
			 
			 return redirect()->route('frontend.login')->with('message','Registration success.Please Login with user name and paassword');
		
		
	}

	public function rlApiAddCustomer($param){
			
		$data_string = json_encode($param);
		echo $data_string;
		
		/*UAT
        https://uatservice.redlogik.com/index.php/r2g-customer-registration */
	    $url='https://stgservice.redlogik.com/index.php/r2g-customer-registration';
		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('RL_SECRET_ACCESS_KEY:$2y$10$XiTvpgMntLU26/PGiWAjJuWDavnMZAjcEVtidBVX/bZ6y4X.97jOW',
		                                           'Content-Type:application/json',
                                                   'Content-Length: ' . strlen($data_string) ));
		
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 300);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$curl_scraped_page = curl_exec($ch);
		var_dump($curl_scraped_page);
		
		curl_close($ch);

		     exit;
		   return(true);

	}

    
}
