<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request; 

class FrontendLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('frontend_guest')->except('logout');
		auth('web')->logout();  // login session isue fix
		
    }
	
	
	protected function credentials(Request $request)
    {
		
		
        $field = 'username';
        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }
	
	
	/**
     * @return property guard use for login
     * 
     */ 
    public function guard()
    {
      return auth()->guard('frontend');
    }

    public function showLoginForm()
    {
        return view('frontend.login.login');
    }
	
	

}
