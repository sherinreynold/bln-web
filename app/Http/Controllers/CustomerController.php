<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvitationEmail;
use App\Mail\adminCreateSuccessEmail;
use App\Frontend;

class CustomerController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $code_filter='';
        $reff_code_filter='';

		if($request->has('code')){
            $code_filter = $request->input('code');
        }

        if($request->has('reff_code')){
            $reff_code_filter = $request->input('reff_code');
        }
        
        $customers = DB::table('frontend_users as customer')->join('frontend_users as parent','parent.id','=','customer.parent_id')
                    ->where('customer.bln_role_id', '4');;
        if($code_filter != ''){
            $customers=  $customers->where('customer.code','like','%'.$code_filter.'%');  
                        
        }
        if($reff_code_filter != ''){
            $customers=  $customers->where('parent.code','like','%'.$reff_code_filter.'%');  
                        
        }


           $customers = $customers->select('customer.*','parent.code as parent_code')
                        ->orderBy('customer.created_at', 'desc')->paginate(10);
        
        //echo '<pre>';print_r($customers);exit;
        $breadcrumbs[0]['title']='Customers';
		
        return view('dashboard.customers.list', ['notes' => $customers,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs[0]['title']='Customers';
        $breadcrumbs[0]['link']=url('admin/customers');
        $breadcrumbs[1]['title']='Add';

        $parents = Frontend::all(['name','id','code','account_status','bln_role_id'])->whereIn('bln_role_id', [ '1','2', '3','4'])
        ->where('account_status','enabled')->sortByDesc("id")->toArray();
       // echo '<pre>';print_r($parents);exit;
        return view('dashboard.customers.create',['breadcrumbs' => $breadcrumbs,'parents' => $parents]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'name'           => 'required',
			'parent'        => 'required',
			'email_id'           => 'required'
        ]);
       
	   	    
			 
		 if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('customers.create')->withErrors('email id already exists');
             }
            
             

        $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->bln_role_id = '4';
		$frontend_user->save();			 
	   
	     
		$code=str_pad($frontend_user->id,10, '0', STR_PAD_LEFT);
	    $user_code="C".$code; 
		 
        $c = Frontend::find($frontend_user->id);
		$c->code = $user_code;
        $c->name     = $request->input('name');
		$c->phone = $request->input('mobile');
        $c->customer_type = $request->input('type');
        $c->parent_id = $request->input('parent');
		
        
       
        
        $c->save();

        $mailData = array(
            'name'     => $request->input('name'),
            'action'     => url('registration/'.base64_encode($user_code)),
        );
        Mail::to($request->input('email_id'))->send(new InvitationEmail($mailData));

        $request->session()->flash('message', 'Registration link sent to '.$request->input('email_id'));
        
		return redirect()->route('customers.index');

	
	
}


}
