<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customer';
	public $timestamps = false;
	
	public function salesgen()
	{
	    return $this->belongsTo('App\Models\SalesGenies', 'ref_code', 'code');
	}

	public function frontend_customers()
	{
	    return $this->belongsTo('App\Models\Frontend', 'fk_frontend_user_id', 'id');
	}
	public function scopeFilter($query, $request)
    {
        if ($request['ref_code']) {
            $query->where('ref_code', '=', $request['ref_code']);
        }
        return $query;
    }
	
	
}
